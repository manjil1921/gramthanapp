package com.pracasinfosys.gramthan.app.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.pracasinfosys.gramthan.app.R;

public class Newscontent_details extends AppCompatActivity {
    TextView details,dates,titless;
    Toolbar mtoolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newscontent_details);
        mtoolbar = (Toolbar)findViewById(R.id.toolbar);
        //String mtitle = getIntent().getExtras().getString("names_1");
        setSupportActionBar(mtoolbar);
       // setTitle(mtitle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mtoolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        details = (TextView)findViewById(R.id.textview_detaislnoticedetails);
        dates = (TextView)findViewById(R.id.textview_detailsnoticedates);
        titless = (TextView)findViewById(R.id.textview_detailsnotices);

        String detials = getIntent().getExtras().getString("detailcontent");
        String datess = getIntent().getExtras().getString("dates");
        String titles = getIntent().getExtras().getString("title");
        details.setText(detials);
        dates.setText(datess);
        titless.setText(titles);
    }
}
