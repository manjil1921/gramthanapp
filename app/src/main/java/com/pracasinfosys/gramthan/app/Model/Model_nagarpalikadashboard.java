package com.pracasinfosys.gramthan.app.Model;

/**
 * Created by Razu on 1/1/2018.
 */

public class Model_nagarpalikadashboard {
    private int  Image;
    private String text;

    public Model_nagarpalikadashboard(int image, String text) {
        Image = image;
        this.text = text;
    }

    public int getImage() {
        return Image;
    }

    public void setImage(int image) {
        Image = image;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
