package com.pracasinfosys.gramthan.app.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pracasinfosys.gramthan.app.Model.Model_notice_array;
import com.pracasinfosys.gramthan.app.R;

import java.util.List;

/**
 * Created by Dell on 2/22/2018.
 */

public class Recyclerview_nagarikbadapatra extends RecyclerView.Adapter<Recyclerview_nagarikbadapatra.MyViewHolder> {
    private Context mcontext;
    private List<Model_notice_array> mgetlist;

    public Recyclerview_nagarikbadapatra(Context mcontext, List<Model_notice_array> mgetlist) {
        this.mcontext = mcontext;
        this.mgetlist = mgetlist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mview = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_updated,parent,false);

        return new MyViewHolder(mview);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public MyViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void onClick(View view) {

        }
    }
}
