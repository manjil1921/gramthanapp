package com.pracasinfosys.gramthan.app.Activity;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.pracasinfosys.gramthan.app.Interface_retrofit;
import com.pracasinfosys.gramthan.app.R;

import java.util.HashMap;
import java.util.Map;

import static com.basgeekball.awesomevalidation.ValidationStyle.COLORATION;
//import retrofit2.Response;

public class Gunasotathasujhav_8 extends AppCompatActivity {
    Toolbar mtolbars;
    EditText msub,mname,mfeedback,maddress,mphone;
    String sub,name,body,phone,address;
    Button msend,mcance;
    Interface_retrofit mapai;
    RequestQueue mrequest;
    AwesomeValidation mAwesomeValidation = new AwesomeValidation(COLORATION);
    // List<Model_notice_main> mlist = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gunasotathasujhav_8);
        mtolbars = (Toolbar)findViewById(R.id.ToolBars);
        setSupportActionBar(mtolbars);
        // setTitle("गुनासो तथा सुझाब");
        mrequest = Volley.newRequestQueue(this);
        mAwesomeValidation.setColor(Color.RED);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        msend =(Button)findViewById(R.id.buttonsend);
        mcance =(Button)findViewById(R.id.buttoncancel);

        mtolbars.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        msub =(EditText) findViewById(R.id.subject);
        //mAwesomeValidation.addValidation(Gunasotathasujhav_8.this, R.id.subject, "[a-zA-Z\\s]+", R.string.err_name);
        mname =(EditText)findViewById(R.id.username);
        // mAwesomeValidation.addValidation(Gunasotathasujhav_8.this, R.id.username, "[a-zA-Z\\s]+", R.string.err_name);
        mfeedback =(EditText)findViewById(R.id.gunaso);
        // mAwesomeValidation.addValidation(Gunasotathasujhav_8.this, R.id.gunaso, "[a-zA-Z\\s]+", R.string.err_name);
        maddress=(EditText)findViewById(R.id.thegana);
        // mAwesomeValidation.addValidation(Gunasotathasujhav_8.this, R.id.thegana, "[a-zA-Z\\s]+", R.string.err_name);
        mphone =(EditText)findViewById(R.id.phone);
        // mAwesomeValidation.addValidation(Gunasotathasujhav_8.this, R.id.phone, "[0-9]+", R.string.err_name);


        msend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = mname.getText().toString();
                sub = msub.getText().toString();
                body = mfeedback.getText().toString();
                phone = mphone.getText().toString();
                address = maddress.getText().toString();
                //mAwesomeValidation.validate();
                if(name.length()>0){
                    mname.setBackgroundResource(R.drawable.edittext_outerline);
                }else{
                    mname.setBackgroundResource(R.drawable.edittext_errorouterline);
                }
                if(sub.length()>0){
                    msub.setBackgroundResource(R.drawable.edittext_outerline);
                }else{
                    msub.setBackgroundResource(R.drawable.edittext_errorouterline);
                }
                if(mfeedback.length()>0){
                    mfeedback.setBackgroundResource(R.drawable.edittext_outerline);
                }else {
                    mfeedback.setBackgroundResource(R.drawable.edittext_errorouterline);
                }
                if(maddress.length()>0){
                    maddress.setBackgroundResource(R.drawable.edittext_outerline);
                }else {
                    maddress.setBackgroundResource(R.drawable.edittext_errorouterline);
                }
                if(phone.length()==10){
                    mphone.setBackgroundResource(R.drawable.edittext_outerline);
                }else {
                    mphone.setBackgroundResource(R.drawable.edittext_errorouterline);
                }
                if(name.length()>0 && sub.length()>0 && mfeedback.length()>0 && phone.length()==10 && maddress.length()>0){
                    voleyrequest();
                    mname.setText(null);
                    mname.setBackgroundResource(R.drawable.edittext_outerline);
                    msub.setText(null);
                    msub.setBackgroundResource(R.drawable.edittext_outerline);
                    mfeedback.setText(null);
                    mfeedback.setBackgroundResource(R.drawable.edittext_outerline);
                    mphone.setText(null);
                    mphone.setBackgroundResource(R.drawable.edittext_outerline);
                    maddress.setText(null);
                    maddress.setBackgroundResource(R.drawable.edittext_outerline);
                    Toast.makeText(Gunasotathasujhav_8.this, "Compliant Sent Successfully!!!", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(Gunasotathasujhav_8.this, "Please Fill Up the Field First!!", Toast.LENGTH_SHORT).show();
                }
                /*if(name.length()>0 || sub.length()>0 || mfeedback.length()>0 || phone.length()>0 || maddress.length()>0 ){
                if(name.length()>0 ){
                    mname.setBackgroundResource(R.drawable.edittext_outerline);
                    if(sub.length()>0 || mfeedback.length()>0 || phone.length()>0 || maddress.length()>0) {
                        msub.setBackgroundResource(R.drawable.edittext_outerline);
                        if(mfeedback.length()>0  || maddress.length()>0) {
                            mfeedback.setBackgroundResource(R.drawable.edittext_outerline);
                            if(maddress.length()>0|| phone.length()>0)
                            {
                                maddress.setBackgroundResource(R.drawable.edittext_outerline);
                                //Toast.makeText(Gunasotathasujhav_8.this, "Please FIll up all field", Toast.LENGTH_SHORT).show();
                                // mAwesomeValidation.clear();
                                if (phone.length() == 10) {
                                    // Toast.makeText(Gunasotathasujhav_8.this, "Invalid Phone No.", Toast.LENGTH_SHORT).show();
                                    //mphone.setText("");
                                    // mphone.setFocusable(true);
                                    //mAwesomeValidation.clear();
                                    //mphone.getEditableText();
                                    // return;
                                    voleyrequest();
                                    mname.setText(null);
                                    mname.setBackgroundResource(R.drawable.edittext_outerline);
                                    msub.setText(null);
                                    msub.setBackgroundResource(R.drawable.edittext_outerline);
                                    mfeedback.setText(null);
                                    mfeedback.setBackgroundResource(R.drawable.edittext_outerline);
                                    mphone.setText(null);
                                    mphone.setBackgroundResource(R.drawable.edittext_outerline);
                                    maddress.setText(null);
                                    maddress.setBackgroundResource(R.drawable.edittext_outerline);
                                    Toast.makeText(Gunasotathasujhav_8.this, "Compliant Sent Successfully!!!", Toast.LENGTH_SHORT).show();
                                } else {
                                    mphone.setBackgroundResource(R.drawable.edittext_errorouterline);
                                    Toast.makeText(Gunasotathasujhav_8.this, "Invalid Phone No:", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                maddress.setBackgroundResource(R.drawable.edittext_errorouterline);
                                mphone.setBackgroundResource(R.drawable.edittext_errorouterline);
                            }
                        }else{
                            mfeedback.setBackgroundResource(R.drawable.edittext_errorouterline);
                            mphone.setBackgroundResource(R.drawable.edittext_errorouterline);
                            maddress.setBackgroundResource(R.drawable.edittext_errorouterline);
                        }
                    }else{
                        msub.setBackgroundResource(R.drawable.edittext_errorouterline);
                        mfeedback.setBackgroundResource(R.drawable.edittext_errorouterline);
                        //Toast.makeText(Gunasotathasujhav_8.this, "Invalid Phone No:", Toast.LENGTH_SHORT).show();
                        mphone.setBackgroundResource(R.drawable.edittext_errorouterline);
                        maddress.setBackgroundResource(R.drawable.edittext_errorouterline);
                    }
                }else {
                     //msub.setBackgroundResource(R.drawable.edittext_errorouterline);
                     mname.setBackgroundResource(R.drawable.edittext_errorouterline);
                    // mfeedback.setBackgroundResource(R.drawable.edittext_errorouterline);
                     //maddress.setBackgroundResource(R.drawable.edittext_errorouterline);
                    msub.setBackgroundResource(R.drawable.edittext_errorouterline);
                   // mname.setBackgroundResource(R.drawable.edittext_errorouterline);
                    mfeedback.setBackgroundResource(R.drawable.edittext_errorouterline);
                    //Toast.makeText(Gunasotathasujhav_8.this, "Invalid Phone No:", Toast.LENGTH_SHORT).show();
                    mphone.setBackgroundResource(R.drawable.edittext_errorouterline);
                    maddress.setBackgroundResource(R.drawable.edittext_errorouterline);
                }}else{
                    msub.setBackgroundResource(R.drawable.edittext_errorouterline);
                    mname.setBackgroundResource(R.drawable.edittext_errorouterline);
                     mfeedback.setBackgroundResource(R.drawable.edittext_errorouterline);
                    //Toast.makeText(Gunasotathasujhav_8.this, "Invalid Phone No:", Toast.LENGTH_SHORT).show();
                     mphone.setBackgroundResource(R.drawable.edittext_errorouterline);
                    maddress.setBackgroundResource(R.drawable.edittext_errorouterline);
                }*/

                //sendEmail();

                //post_compliant_restdata();
            }
        });
        mcance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAwesomeValidation.clear();
                finish();
            }
        });
    }
    public void voleyrequest(){

        Log.d("v", "voleyrequest: nnnnnnnnnnnn"+name+sub+body+phone+address);
        String url = "https://pracasinfosys.com/gramthan/api/complaint";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("success", "onResponse: sssssssssssss"+response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("fai", "onResponse: sssssssssssss"+error.toString());
            }
        }){
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("action", "save");
                params.put("name", name);
                params.put("phone", phone);
                params.put("address", address);
                params.put("sub", sub);
                params.put("body",body);
                return params;
            }
        };
        mrequest.add(postRequest);
    }
   /* public void post_compliant_restdata(){
        String body = feedback;
        mapai = Apiclient.getApiclient().create(Interface_retrofit.class);
        Call<Model_post_data>  mpostdata = mapai.mpostfeedback(name,phone,address,sub,body);
        mpostdata.enqueue(new Callback<Model_post_data>() {
            @Override
            public void onResponse(Call<Model_post_data> call, Response<Model_post_data> response) {
              String  mlist = response.body().getAck().toString();
                Toast.makeText(Gunasotathasujhav_8.this, mlist, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Model_post_data> call, Throwable t) {
                Log.d("falied", "onFailure: failes"+t.toString());
                Toast.makeText(Gunasotathasujhav_8.this, "failed", Toast.LENGTH_SHORT).show();
            }
        });
    }*/

    @Override
    public void onBackPressed() {
        finish();
    }
    /*  protected void sendEmail() {
        //Log.i("Send email", "");
        String[] TO = {"razubhattarai88@gmail.com"};
        String[] CC = {email};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Budiganga Email");
        emailIntent.putExtra(Intent.EXTRA_TEXT, feedback);

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
            Log.i("Finishending email...", "");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(Gunasotathasujhav_8.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }*/
}
