package com.pracasinfosys.gramthan.app;

import com.pracasinfosys.gramthan.app.Model.Model_notice_main;
import com.pracasinfosys.gramthan.app.Model.Model_post_data;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by user on 1/4/2018.
 */

public interface Interface_retrofit {
    @GET("getNews")
    Call<Model_notice_main> mgetnews();
    //@GET("getNotice?sem_id=3")
    //Call<Model_notice_main> mgetnotices();
    @GET("getPublicPurchase")
    Call<Model_notice_main> mgetsarbajanik();
    @GET("getTaxCost")
    Call<Model_notice_main> mgettaxcost();
    @GET("getApplicationFormat")
    Call<Model_notice_main> mgetnibedankodhacha();
    @GET("getCitizenCharter")
    Call<Model_notice_main> mgetbadapatra();
    @GET("getSocialSecurity")
    Call<Model_notice_main> mgetsocialsecurity(@Query("ward_id") String ward_id);
    @GET("getStaffDetails")
    Call<Model_notice_main> mgetbibaran(@Query("ward_id") String ward_id);
    @GET("getContactDetails")
    Call<Model_notice_main> mgetinfo(@Query("ward_id") String ward_id);
    @GET("getSocialSecurityDetail")
    Call<Model_notice_main> mgetsecutity_details();
    @POST("complaint")
    @FormUrlEncoded
    Call<Model_post_data> mpostfeedback(@Field("name") String name, @Field("phone") String phone, @Field("address") String address, @Field("sub") String sub, @Field("body") String body);
    @GET("getOffice")
    Call<Model_notice_main> mgetimportantno();
    @GET("getVitalRegistration")
    Call<Model_notice_main> mgetyaktibibarans();
    @GET("getConsumer")
    Call<Model_notice_main> mgetconsumer();
    @GET("getPersonalSecurity")
    Call<Model_notice_main> mgetpersonalsecurity();
    @GET("getEmail")
    Call<Model_notice_main> mgetEmail();
    @GET("getNumbers")
    Call<Model_notice_main> mgetdetailss(@Query("office_id") String office_id);
    @GET("getWardSocial")
    Call<Model_notice_main> mgetwardsofficeial();
    @GET("getWards")
    Call<Model_notice_main> mgetward_samparka();

}
