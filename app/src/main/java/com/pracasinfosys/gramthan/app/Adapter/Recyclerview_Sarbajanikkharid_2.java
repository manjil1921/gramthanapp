package com.pracasinfosys.gramthan.app.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.pracasinfosys.gramthan.app.Activity.Printing;
import com.pracasinfosys.gramthan.app.Activity.Printingactivity;
import com.pracasinfosys.gramthan.app.Downloadclass;
import com.pracasinfosys.gramthan.app.Model.Model_notice_array;
import com.pracasinfosys.gramthan.app.R;
import com.pracasinfosys.gramthan.app.Utils;
import com.jacksonandroidnetworking.JacksonParserFactory;

import java.util.List;

/**
 * Created by user on 1/5/2018.
 */

public class Recyclerview_Sarbajanikkharid_2 extends RecyclerView.Adapter<Recyclerview_Sarbajanikkharid_2.MyViewHolder> {
    List<Model_notice_array> mkharid;
    private Context mcontext;
    String acttivity_url;
    Downloadclass mclass;
    Printingactivity mprintingclass;
    ProgressDialog mdialogue;

    public Recyclerview_Sarbajanikkharid_2(List<Model_notice_array> mkharid, Context mcontext,String acttivity_url) {
        this.mkharid = mkharid;
        this.mcontext = mcontext;
        this.acttivity_url = acttivity_url;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mview = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_sarbajanik_2,parent,false);
        return new MyViewHolder(mview);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
    Model_notice_array marray = mkharid.get(position);
        holder.mtextview.setText(marray.getTitle());

    }

    @Override
    public int getItemCount() {
        return mkharid.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{

       TextView mtextview,mtextviewdownload,mtextviewview,mtextview_print;
       ImageView mdownload,mviews,mimage_print;

        public MyViewHolder(View itemView) {
            super(itemView);
            mtextview =(TextView)itemView.findViewById(R.id.textview_1_newstitle_sarbajanik);
            mtextviewdownload =(TextView)itemView.findViewById(R.id.textview_downloads);
            mtextviewview =(TextView)itemView.findViewById(R.id.textview_downloadss);
            mdownload=(ImageView)itemView.findViewById(R.id.view_download);
            mviews =(ImageView)itemView.findViewById(R.id.view_views);
            mtextview_print =(TextView)itemView.findViewById(R.id.textview_print);
            mimage_print=(ImageView)itemView.findViewById(R.id.imageview_print);
            //printing
            mimage_print.setOnClickListener(this);
            //mtextview_print.setOnClickListener(this);

            //
            mtextviewview.setOnClickListener(this);
            mdownload.setOnClickListener(this);
            mviews.setOnClickListener(this);
            mtextviewdownload.setOnClickListener(this);
            mtextview.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = getAdapterPosition();
            mdialogue = new ProgressDialog(view.getContext());
            AndroidNetworking.initialize(mcontext);
            AndroidNetworking.setParserFactory(new JacksonParserFactory());
            switch (view.getId()){
                case R.id.imageview_print:
                   // progressdialogue("Printing.Please Wait....");
                    if(acttivity_url.equals("taxcost")) {

                      // mprintingclass = new Printingactivity(mcontext, mkharid.get(id).getFile(),"taxcost");
                        Toast.makeText(mcontext, "Loading.Please wait....", Toast.LENGTH_SHORT).show();
                        String namess = "kar_"+id+".pdf";
                        Intent mintess = new Intent(view.getContext(), Printing.class);
                        mintess.putExtra("files",mkharid.get(id).getFile());
                        mintess.putExtra("filename",namess);
                        view.getContext().startActivity(mintess);
                    }
                    else if(acttivity_url.equals("personalsecurity")){
                //mprintingclass = new Printingactivity(mcontext,mkharid.get(id).getFile(),"personalsecurity");
                        Toast.makeText(mcontext, "Loading.Please wait....", Toast.LENGTH_SHORT).show();
                        String namess = "nagariksurakshya_"+id+".pdf";
                        Intent mintess = new Intent(view.getContext(), Printing.class);
                        mintess.putExtra("files",mkharid.get(id).getFile());
                        mintess.putExtra("filename",namess);
                        view.getContext().startActivity(mintess);
            }else if(acttivity_url.equals("ApplicationFormat")){
                        Toast.makeText(mcontext, "Loading.Please wait....", Toast.LENGTH_SHORT).show();
                //Intent mintens = new Intent(Intent.ACTION_VIEW, Uri.parse(mkharid.get(id).getFile()));
               // mprintingclass = new Printingactivity(mcontext,mkharid.get(id).getFile(),"ApplicationFormat");
                        String namess = "nibedankodacha_"+id+".pdf";
                        Intent mintess = new Intent(view.getContext(), Printing.class);
                        mintess.putExtra("files",mkharid.get(id).getFile());
                        mintess.putExtra("filename",namess);
                        view.getContext().startActivity(mintess);
            }else if(acttivity_url.equals("vital_registration")){
                        Toast.makeText(mcontext, "Loading.Please wait....", Toast.LENGTH_SHORT).show();
               // mprintingclass = new Printingactivity(mcontext,mkharid.get(id).getFile(),"vital_registration");
                //mitnent.putExtra("file",mnagarik.get(id).getFile());
                       /* AndroidNetworking.download(mkharid.get(id).getFile(), Environment.getExternalStorageDirectory()+"/"+Utils.downloadDirectory,"vital_registration.pdf")
                                .setTag("downloadTest")
                                .setPriority(Priority.MEDIUM)
                                .build()
                                .setDownloadProgressListener(new DownloadProgressListener() {
                                    @Override
                                    public void onProgress(long bytesDownloaded, long totalBytes) {
                                        // do anything with progress
                                        Toast.makeText(mcontext, "Downloading", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .startDownload(new DownloadListener() {
                                    @Override
                                    public void onDownloadComplete() {
                                        // do anything after completion
                                        Toast.makeText(mcontext, "Finished", Toast.LENGTH_SHORT).show();
                                        //PrintManager printManager = (PrintManager)mcontext.getSystemService(Context.PRINT_SERVICE);
                                        //String jobName =mcontext.getString(R.string.app_name) +
                                                //" Document";
                                       // printManager.print(jobName, new MyPrintDocumentAdapter(mcontext,"vital_registration.pdf"),null);
                                    }
                                    @Override
                                    public void onError(ANError error) {
                                        // handle error
                                        Toast.makeText(mcontext, "errpr"+error.toString(), Toast.LENGTH_SHORT).show();
                                        Log.d("ll", "onError: errorddddddddddddddddd"+error.toString());
                                    }
                                });*/
                       String namess = "byaktigarbibaran_"+id+".pdf";
                       Intent mintess = new Intent(view.getContext(), Printing.class);
                       mintess.putExtra("files",mkharid.get(id).getFile());
                       mintess.putExtra("filename",namess);
                       view.getContext().startActivity(mintess);

            }else if (acttivity_url.equals("consumer")){
                        Toast.makeText(mcontext, "Loading.Please wait....", Toast.LENGTH_SHORT).show();
                //mprintingclass = new Printingactivity(mcontext,mkharid.get(id).getFile(),"consumer");
                        String namess = "upavoktasamiti_"+id+".pdf";
                        Intent mintess = new Intent(view.getContext(), Printing.class);
                        mintess.putExtra("files",mkharid.get(id).getFile());
                        mintess.putExtra("filename",namess);
                        view.getContext().startActivity(mintess);

            }else{
                Toast.makeText(mcontext, "please click selected item", Toast.LENGTH_SHORT).show();

            }//Intent i = new Intent(Intent.ACTION_VIEW);
                    //i.setPackage("com.dynamixsoftware.printershare");
                   //i.setDataAndType(Uri.parse(mkharid.get(id).getFile()));
                   // view.getContext().startActivity(i);

                    //PrintManager printManager = (PrintManager)mcontext.getSystemService(Context.PRINT_SERVICE);
                   // String jobName =mcontext.getString(R.string.app_name) +
                            //" Document";
                  //  printManager.print(jobName, new MyPrintDocumentAdapter(mcontext,mkharid.get(id).getFile()),null);
                     break;
                case R.id.textview_downloads:
                   // progressdialogue("Downloading.Please Wait....");
                    if(acttivity_url.equals("taxcost")){
                        //Intent mintens = new Intent(Intent.ACTION_VIEW, Uri.parse(mkharid.get(id).getFile()));
                        //view.getContext().startActivity(mintens);
                        String downloadurl = mkharid.get(id).getFile();
                        String downloadFileName = downloadurl.replace(Utils.mainUrl_taxcost, "");
                        //Toast.makeText(mcontext, "Downloading ..Please Wait.", Toast.LENGTH_SHORT).show();
                       // progressdialogue("Downloading..Please Wait..");
                        downloadingtask_files(downloadurl,downloadFileName);
                    }else if(acttivity_url.equals("personalsecurity")){
                        String downloadurl = mkharid.get(id).getFile();
                        String downloadFileName = downloadurl.replace(Utils.mainUrl_personalsecurity, "");
                        //Toast.makeText(mcontext, "Downloading ..Please Wait.", Toast.LENGTH_SHORT).show();
                       // progressdialogue("Downloading..Please Wait..");
                        downloadingtask_files(downloadurl,downloadFileName);
                    }else if(acttivity_url.equals("ApplicationFormat")){
                        //Intent mintens = new Intent(Intent.ACTION_VIEW, Uri.parse(mkharid.get(id).getFile()));
                        String downloadurl = mkharid.get(id).getFile();
                        String downloadFileName = downloadurl.replace(Utils.mainUrl_applicationformat, "");
                        //Toast.makeText(mcontext, "Downloading ..Please Wait.", Toast.LENGTH_SHORT).show();
                        //progressdialogue("Downloading..Please Wait..");
                        downloadingtask_files(downloadurl,downloadFileName);
                    }else if(acttivity_url.equals("vital_registration")){
                        String downloadurl = mkharid.get(id).getFile();
                        String downloadFileName = downloadurl.replace(Utils.mainUrl_vitalregistration, "");
                        //Toast.makeText(mcontext, "Downloading ..Please Wait.", Toast.LENGTH_SHORT).show();
                       // progressdialogue("Downloading..Please Wait..");
                        downloadingtask_files(downloadurl,downloadFileName);
                        //mitnent.putExtra("file",mnagarik.get(id).getFile());

                    }else if (acttivity_url.equals("consumer")){
                        String downloadurl = mkharid.get(id).getFile();
                        String downloadFileName = downloadurl.replace(Utils.mainUrl_consumer, "");
                        //Toast.makeText(mcontext, "Downloading ..Please Wait.", Toast.LENGTH_SHORT).show();
                        //progressdialogue("Downloading..Please Wait..");
                        downloadingtask_files(downloadurl,downloadFileName);


                    }else{
                        Toast.makeText(mcontext, "please click selected item", Toast.LENGTH_SHORT).show();

                    }
                break;
                case R.id.textview_downloadss:
                    if(acttivity_url.equals("taxcost")){
                        Intent mintens = new Intent(Intent.ACTION_VIEW, Uri.parse(mkharid.get(id).getFile()));
                        view.getContext().startActivity(mintens);
                        //Downloadclass mclass = new Downloadclass(mcontext,mkharid.get(id).getFile(),"taxcost");
                    }else if(acttivity_url.equals("personalsecurity")){
                        Intent mintens = new Intent(Intent.ACTION_VIEW, Uri.parse(mkharid.get(id).getFile()));
                        view.getContext().startActivity(mintens);
                    }else if(acttivity_url.equals("ApplicationFormat")){
                        Intent mintens = new Intent(Intent.ACTION_VIEW, Uri.parse(mkharid.get(id).getFile()));
                        view.getContext().startActivity(mintens);
                        //Downloadclass mclass = new Downloadclass(mcontext,mkharid.get(id).getFile(),"ApplicationFormat");
                    }else if(acttivity_url.equals("vital_registration")){
                        Intent mintens = new Intent(Intent.ACTION_VIEW, Uri.parse(mkharid.get(id).getFile()));
                        view.getContext().startActivity(mintens);
                    }else if (acttivity_url.equals("consumer")){
                        Intent mintens = new Intent(Intent.ACTION_VIEW, Uri.parse(mkharid.get(id).getFile()));
                        view.getContext().startActivity(mintens);

                    }else {
                        Toast.makeText(mcontext, "please click selected item", Toast.LENGTH_SHORT).show();

                    }
                    break;
                case R.id.view_download:
                    //progressdialogue("Downloading.Please Wait....");
                    if(acttivity_url.equals("taxcost")){
                        //Intent mintens = new Intent(Intent.ACTION_VIEW, Uri.parse(mkharid.get(id).getFile()));
                        //view.getContext().startActivity(mintens);
                       //mclass = new Downloadclass(mcontext,mkharid.get(id).getFile(),"taxcost");
                        String downloadurl = mkharid.get(id).getFile();
                        String downloadFileName = downloadurl.replace(Utils.mainUrl_taxcost, "");
                        //Toast.makeText(mcontext, "Downloading ..Please Wait.", Toast.LENGTH_SHORT).show();
                        //progressdialogue("Downloading..Please Wait..");
                        downloadingtask_files(downloadurl,downloadFileName);
                    }else if(acttivity_url.equals("personalsecurity")){
                        String downloadurl = mkharid.get(id).getFile();
                        String downloadFileName = downloadurl.replace(Utils.mainUrl_personalsecurity, "");
                       // Toast.makeText(mcontext, "Downloading ..Please Wait.", Toast.LENGTH_SHORT).show();
                        //progressdialogue("Downloading..Please Wait..");
                        downloadingtask_files(downloadurl,downloadFileName);
                        //mclass = new Downloadclass(mcontext,mkharid.get(id).getFile(),"personalsecurity");
                    }else if(acttivity_url.equals("ApplicationFormat")){
                        //Intent mintens = new Intent(Intent.ACTION_VIEW, Uri.parse(mkharid.get(id).getFile()));
                        String downloadurl = mkharid.get(id).getFile();
                        String downloadFileName = downloadurl.replace(Utils.mainUrl_applicationformat, "");
                        //Toast.makeText(mcontext, "Downloading ..Please Wait.", Toast.LENGTH_SHORT).show();
                        //progressdialogue("Downloading..Please Wait..");
                        downloadingtask_files(downloadurl,downloadFileName);
                       //mclass = new Downloadclass(mcontext,mkharid.get(id).getFile(),"ApplicationFormat");
                    }else if(acttivity_url.equals("vital_registration")){
                        String downloadurl = mkharid.get(id).getFile();
                        String downloadFileName = downloadurl.replace(Utils.mainUrl_vitalregistration, "");
                        //Toast.makeText(mcontext, "Downloading ..Please Wait.", Toast.LENGTH_SHORT).show();
                        //progressdialogue("Downloading..Please Wait..");
                        downloadingtask_files(downloadurl,downloadFileName);
                        //mclass = new Downloadclass(mcontext,mkharid.get(id).getFile(),"vital_registration");
                    }else if (acttivity_url.equals("consumer")){
                        String downloadurl = mkharid.get(id).getFile();
                        String downloadFileName = downloadurl.replace(Utils.mainUrl_consumer, "");
                        //Toast.makeText(mcontext, "Downloading ..Please Wait.", Toast.LENGTH_SHORT).show();
                        //progressdialogue("Downloading..Please Wait..");
                        downloadingtask_files(downloadurl,downloadFileName);
                       // mclass = new Downloadclass(mcontext,mkharid.get(id).getFile(),"consumer");
                        }else{
                        Toast.makeText(mcontext, "please click selected item", Toast.LENGTH_SHORT).show();

                    }
                    break;
                case R.id.view_views:
                            if(acttivity_url.equals("taxcost")){
                                Intent mintens = new Intent(Intent.ACTION_VIEW, Uri.parse(mkharid.get(id).getFile()));
                                view.getContext().startActivity(mintens);

                        //Downloadclass mclass = new Downloadclass(mcontext,mkharid.get(id).getFile(),"taxcost");
                    }else if(acttivity_url.equals("personalsecurity")){
                        Intent mintens = new Intent(Intent.ACTION_VIEW, Uri.parse(mkharid.get(id).getFile()));
                        view.getContext().startActivity(mintens);
                    }else if(acttivity_url.equals("ApplicationFormat")){
                        Intent mintens = new Intent(Intent.ACTION_VIEW, Uri.parse(mkharid.get(id).getFile()));
                        view.getContext().startActivity(mintens);
                        //Downloadclass mclass = new Downloadclass(mcontext,mkharid.get(id).getFile(),"ApplicationFormat");
                    }else if(acttivity_url.equals("vital_registration")){
                        Intent mintens = new Intent(Intent.ACTION_VIEW, Uri.parse(mkharid.get(id).getFile()));
                        view.getContext().startActivity(mintens);
                    }else if (acttivity_url.equals("consumer")){
                        Intent mintens = new Intent(Intent.ACTION_VIEW, Uri.parse(mkharid.get(id).getFile()));
                        view.getContext().startActivity(mintens);

                    }else {
                        Toast.makeText(mcontext, "please click selected item", Toast.LENGTH_SHORT).show();

                    }
                    break;

            }

        }
    }
    public void progressdialogue(String messages){

        //mdialogue = new ProgressDialog(this);
        mdialogue.setMessage(messages);
        mdialogue.setIndeterminate(false);
        mdialogue.setCancelable(false);
        mdialogue.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mdialogue.setProgress(10);
        mdialogue.show();
        //Handler mahand = new Handler();
       // mahand.postDelayed(new Runnable() {
           // @Override
           // public void run() {
               // mdialogue.dismiss();
          //  }
       // },4000);
    }
    public void downloadingtask_files(String fileurl,String flenamses){
        Log.d("dd", "downloadingtask_files: ffffffffffffffffffff"+fileurl);
        AndroidNetworking.download(fileurl, Environment.getExternalStorageDirectory()+"/"+ Utils.downloadDirectory,flenamses)
                .setTag("downloadTest")
                .setPriority(Priority.MEDIUM)
                .build()
                .setDownloadProgressListener(new DownloadProgressListener() {
                    @Override
                    public void onProgress(long bytesDownloaded, long totalBytes) {
                        // do anything with progress
                        //new Handler().postDelayed(new Runnable() {
                            //@Override
                        progressdialogue("Downloading...Please Wait..");
                            //public void run() {
                                //Toast.makeText(mcontext, "Downloading..Please Wait..", Toast.LENGTH_SHORT).show();
                           // }
                        //},5000);

                    }
                })
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        // do anything after completion
                        mdialogue.dismiss();
                        Toast.makeText(mcontext, "Download Finished", Toast.LENGTH_SHORT).show();
                        //PrintManager printManager = (PrintManager)mcontext.getSystemService(Context.PRINT_SERVICE);
                        //String jobName =mcontext.getString(R.string.app_name) +
                        //" Document";
                        // printManager.print(jobName, new MyPrintDocumentAdapter(Printing.this,getfilename),null);
                        // finish();
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        mdialogue.dismiss();
                        Toast.makeText(mcontext, "Failed To Download..Please Try Again Later!!", Toast.LENGTH_SHORT).show();
                        Log.d("ll", "onError: errorddddddddddddddddd"+error.toString());
                    }
                });

    }
}
