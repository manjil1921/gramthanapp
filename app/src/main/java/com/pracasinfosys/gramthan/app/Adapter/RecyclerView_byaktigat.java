package com.pracasinfosys.gramthan.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pracasinfosys.gramthan.app.Activity.Nagarikbadapatra_detials;
import com.pracasinfosys.gramthan.app.Activity.Sarbajanik_2_details;
import com.pracasinfosys.gramthan.app.Downloadclass;
import com.pracasinfosys.gramthan.app.Model.Model_notice_array;
import com.pracasinfosys.gramthan.app.R;

import java.util.List;

/**
 * Created by user on 1/8/2018.
 */

public class RecyclerView_byaktigat extends RecyclerView.Adapter<RecyclerView_byaktigat.MyViewHolder>{
    List<Model_notice_array> mlist;
    private Context mcontext;
    String bibaran;
    Downloadclass mclass;
    public RecyclerView_byaktigat(List<Model_notice_array> mlist, Context mcontext,String bibaran) {
        this.mlist = mlist;
        this.mcontext = mcontext;
        this.bibaran = bibaran;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mview = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_byaktigatbibaran,parent,false);

        return new MyViewHolder(mview);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
    Model_notice_array marray = mlist.get(position);
    holder.mtextview.setText(marray.getTitle());

    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView mtextview;
        public MyViewHolder(View itemView) {
            super(itemView);
            mtextview =(TextView)itemView.findViewById(R.id.textview_byaktigat);
            itemView.setOnClickListener(this);
            mtextview.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = getAdapterPosition();
            if(bibaran.equals("personalsecurity")){
         // mclass = new Downloadclass(mcontext,mlist.get(getAdapterPosition()).getFile(),"personalsecurity");
        }else if(bibaran.equals("vital_registration")) {
         // mclass = new Downloadclass(mcontext,mlist.get(getAdapterPosition()).getFile(),"vital_registration");

            }else if(bibaran.equals("publicpurchase")) {
                //mclass = new Downloadclass(mcontext,mlist.get(getAdapterPosition()).getFile(),"publicpurchase");
                Intent mitnent = new Intent(view.getContext(), Sarbajanik_2_details.class);
                mitnent.putExtra("content",mlist.get(id).getContent());
                mitnent.putExtra("tolbar_text","सार्बजनिक खरिद");
                mitnent.putExtra("file",mlist.get(id).getFile());
                mitnent.putExtra("downloadfile","Sarbajanikkharid.pdf");
               // mitnent.putExtra("file",mlist.get(id).getFile());
                view.getContext().startActivity(mitnent);
            }else if(bibaran.equals("NagarikBadapatra")) {
              //  mclass = new Downloadclass(mcontext,mlist.get(getAdapterPosition()).getFile(),"consumer");
                Intent mitnent = new Intent(view.getContext(), Nagarikbadapatra_detials.class);
                mitnent.putExtra("content",mlist.get(id).getDescription());
                mitnent.putExtra("title",mlist.get(id).getTitle());
                //mitnent.putExtra("titles","नागरिक बडापत्र");
                //mitnent.putExtra("file",mnagarik.get(id).getFile());
                view.getContext().startActivity(mitnent);
            }else{
               // mclass = new Downloadclass(mcontext,mlist.get(getAdapterPosition()).getFile(),"consumer");

            }
        }
    }
}
