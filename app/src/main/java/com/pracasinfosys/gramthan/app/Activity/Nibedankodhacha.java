package com.pracasinfosys.gramthan.app.Activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.pracasinfosys.gramthan.app.Adapter.Recyclerview_Sarbajanikkharid_2;
import com.pracasinfosys.gramthan.app.Adapter.Recyclerview_Suchanasamachar;
import com.pracasinfosys.gramthan.app.Interface_retrofit;
import com.pracasinfosys.gramthan.app.Model.Model_notice_array;
import com.pracasinfosys.gramthan.app.Model.Model_notice_main;
import com.pracasinfosys.gramthan.app.R;
import com.pracasinfosys.gramthan.app.Restapi.Apiclient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Nibedankodhacha extends AppCompatActivity {
    Interface_retrofit minterface;
    RecyclerView mrecycler;
    RecyclerView.LayoutManager mlayoutmanager;
    Recyclerview_Suchanasamachar madapter;
    SwipeRefreshLayout mswiperefresh;
    List<Model_notice_array> mlist_sarbajanik = new ArrayList<>();
    Toolbar mtolbars;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nibedankodhacha);
        mtolbars = (Toolbar)findViewById(R.id.ToolBars);
        mrecycler=(RecyclerView)findViewById(R.id.mrecyclerview);
        mswiperefresh =(SwipeRefreshLayout)findViewById(R.id.mswiprefresh);
        mswiperefresh.setRefreshing(true);
        setSupportActionBar(mtolbars);
        //setTitle("निबेदन को ढाचा");
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mtolbars.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mlayoutmanager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mrecycler.setLayoutManager(mlayoutmanager);
        getreestapi_sarbajanikkharid();
    }
    public void getreestapi_sarbajanikkharid(){
        minterface = Apiclient.getApiclient().create(Interface_retrofit.class);
        Call<Model_notice_main> msarbajanik = minterface.mgetnibedankodhacha();
        msarbajanik.enqueue(new Callback<Model_notice_main>() {
            @Override
            public void onResponse(Call<Model_notice_main> call, Response<Model_notice_main> response) {
                mlist_sarbajanik = response.body().getResult();
                Log.d("suc", "onResponse: kkkkkkkkkkk"+mlist_sarbajanik);
                mrecycler.setAdapter(new Recyclerview_Sarbajanikkharid_2(mlist_sarbajanik,getApplicationContext(),"ApplicationFormat"));
                mswiperefresh.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<Model_notice_main> call, Throwable t) {
                Log.d("failed", "onResponse: kkkkkkkkkkk"+t.toString());
            }
        });
    }
    }

