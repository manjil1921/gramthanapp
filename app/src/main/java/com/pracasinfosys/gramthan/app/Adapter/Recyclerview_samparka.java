package com.pracasinfosys.gramthan.app.Adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.pracasinfosys.gramthan.app.Activity.Main2Activity;
import com.pracasinfosys.gramthan.app.Model.Model_notice_array;
import com.pracasinfosys.gramthan.app.R;

import java.util.List;

/**
 * Created by Razu on 1/6/2018.
 */

public class Recyclerview_samparka extends RecyclerView.Adapter<Recyclerview_samparka.MyViewHolder> {
    List<Model_notice_array> mlist;
    private Context mcontx;

    public Recyclerview_samparka(List<Model_notice_array> mlist, Context mcontx) {
        this.mlist = mlist;
        this.mcontx = mcontx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mview = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_samparkano,parent,false);
        return new MyViewHolder(mview);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Model_notice_array marray = mlist.get(position);
        holder.mwardno.setText(marray.getWard_no());
        holder.mphone.setText(marray.getPhone());
        holder.memail.setText(marray.getEmail());
        holder.maddress.setText(marray.getAddress());
        holder.mname.setText(marray.getName());
        holder.mphone_2.setText(marray.getPhone1());

        WebSettings webViewSettings = holder.wb.getSettings();
        webViewSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webViewSettings.setJavaScriptEnabled(true);
        //webViewSettings.setPluginsEnabled(true);
        webViewSettings.setBuiltInZoomControls(false);
        webViewSettings.setPluginState(WebSettings.PluginState.ON);
        //webViewSettings.setPluginsEnabled(true);
        holder.wb.getSettings().setJavaScriptEnabled(true);
        String getiframe = "<iframe "+marray.getIframe()+"></iframe>";
        holder.wb.loadData(getiframe,"text/html",
                "utf-8");
        Log.d("iframe", "onBindViewHolder: iframessssssssssssssssssss"+marray.getIframe());
        holder.wb.setWebViewClient(new WebViewClient());

    }

    @Override
    public int getItemCount() {
        return mlist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView mname, maddress, memail, mphone, mwardno,mphone_2;
        WebView wb;
        public MyViewHolder(View itemView) {
            super(itemView);
            wb = (WebView)itemView.findViewById(R.id.webview);
            mname = (TextView) itemView.findViewById(R.id.mname);
            maddress = (TextView) itemView.findViewById(R.id.textview_address);
            memail = (TextView) itemView.findViewById(R.id.textview_email);
            mphone = (TextView) itemView.findViewById(R.id.textview_phone);
            mwardno = (TextView) itemView.findViewById(R.id.mtextview_wardno);
            mphone_2 =(TextView)itemView.findViewById(R.id.textview_phone1);
            mphone_2.setOnClickListener(this);
            mphone.setOnClickListener(this);
            memail.setOnClickListener(this);
            maddress.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = getAdapterPosition();
            String dial = "tel:" + mlist.get(id).getPhone1();
            switch (view.getId()) {
                case R.id.textview_phone1:
                    Intent callIntents = new Intent(Intent.ACTION_CALL);
                    callIntents.setData(Uri.parse(dial));

                    if (ActivityCompat.checkSelfPermission(mcontx, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;

                    }
                    view.getContext().startActivity(callIntents);
                    break;
                case R.id.textview_address:
                    Intent minten = new Intent(view.getContext(), Main2Activity.class);
                    minten.putExtra("latitude",mlist.get(id).getLatitude());
                    minten.putExtra("longitude",mlist.get(id).getLongitude());
                    view.getContext().startActivity(minten);
                    // Create a Uri from an intent string. Use the result to create an Intent.
                    //Uri gmmIntentUri = Uri.parse("https://www.google.com/maps?q=27.6954514,85.3265043");

// Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
                   // Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
// Make the Intent explicit by setting the Google Maps package
                   // mapIntent.setPackage("com.google.android.apps.maps");

// Attempt to start an activity that can handle the Intent
//
                   // view.getContext().startActivity(mapIntent);
                    break;
                case R.id.textview_phone:
                    String dials = "tel:" + mlist.get(id).getPhone();
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse(dials));
                    if (ActivityCompat.checkSelfPermission(mcontx, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;

                    }
                    view.getContext().startActivity(callIntent);
                    break;
                case R.id.textview_email:
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);

                    emailIntent.setData(Uri.parse("mailto:"));
                    emailIntent.setType("text/plain");
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, mlist.get(id).getEmail());
                    emailIntent.putExtra(Intent.EXTRA_CC, "");
                    emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                    emailIntent.putExtra(Intent.EXTRA_TEXT, "");

                    try {
                        view.getContext().startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                        Log.i("Finishending email...", "");
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(mcontx, "There is no email client installed.", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    }
    protected void sendEmail(String email) {
        //Log.i("Send email", "");
        String[] TO = {email};
        String[] CC = {};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, "");
        emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");

        try {
            mcontx.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            Log.i("Finishending email...", "");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(mcontx, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }
}
