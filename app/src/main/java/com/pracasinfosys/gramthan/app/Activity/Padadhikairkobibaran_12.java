package com.pracasinfosys.gramthan.app.Activity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.pracasinfosys.gramthan.app.Adapter.Recyclerview_padhadhikarikobibaran;
import com.pracasinfosys.gramthan.app.Interface_retrofit;
import com.pracasinfosys.gramthan.app.Model.Model_notice_array;
import com.pracasinfosys.gramthan.app.Model.Model_notice_main;
import com.pracasinfosys.gramthan.app.R;
import com.pracasinfosys.gramthan.app.Restapi.Apiclient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Padadhikairkobibaran_12 extends AppCompatActivity {
    private Toolbar toolbar;
    //private TabLayout tabLayout;
    //private ViewPager viewPager;
    Interface_retrofit minterface;
    RecyclerView mrecycler;
    RecyclerView.LayoutManager mlayoutmanager;
    //Recyclerview_Suchanasamachar madapter;
    SwipeRefreshLayout mswiperefresh;
    List<Model_notice_array> mlist_sarbajanik = new ArrayList<>();
    String getoda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_gaaupalika);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //setTitle("पधादीकारीको विवरण");
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        getoda = getIntent().getExtras().getString("oda");
        /* viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);*/
        mrecycler=(RecyclerView)findViewById(R.id.mrecyclerview);
        mswiperefresh =(SwipeRefreshLayout)findViewById(R.id.mswiprefresh);
        mswiperefresh.setRefreshing(true);
        mswiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mswiperefresh.setRefreshing(true);
                if(isconnectd()){
                    getreestapi_sarbajanikkharid();
                    Toast.makeText(Padadhikairkobibaran_12.this,"Refresh Successfull",Toast.LENGTH_SHORT).show();
                    mswiperefresh.setRefreshing(false);
                }else{
                    //mlinear.setVisibility(View.VISIBLE);
                    Toast.makeText(Padadhikairkobibaran_12.this,"Refresh Successfull",Toast.LENGTH_SHORT).show();
                    mswiperefresh.setRefreshing(false);
                }

            }
        });
        mlayoutmanager = new LinearLayoutManager(Padadhikairkobibaran_12.this,LinearLayoutManager.VERTICAL,false);
        mrecycler.setLayoutManager(mlayoutmanager);
        getreestapi_sarbajanikkharid();





    }
    public boolean isconnectd(){
        ConnectivityManager mconn = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo minfo =mconn.getActiveNetworkInfo();
        if(minfo != null && minfo.isConnected()){
            return true;
        }
        return false;
    }
    public void getreestapi_sarbajanikkharid(){
        String ward_id = getoda;
        minterface = Apiclient.getApiclient().create(Interface_retrofit.class);
        Call<Model_notice_main> msarbajanik = minterface.mgetbibaran(ward_id);
        msarbajanik.enqueue(new Callback<Model_notice_main>() {
            @Override
            public void onResponse(Call<Model_notice_main> call, Response<Model_notice_main> response) {
                mlist_sarbajanik = response.body().getResult();
                Log.d("suc", "onResponse: kkkkkkkkkkk"+mlist_sarbajanik);
                mrecycler.setAdapter(new Recyclerview_padhadhikarikobibaran(mlist_sarbajanik,getApplicationContext()));
                mswiperefresh.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<Model_notice_main> call, Throwable t) {
                Log.d("failed", "onResponse: kkkkkkkkkkk"+t.toString());
            }
        });
    }
   /* private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Fragment_gaaupalika(), "Gaaupalika");
        adapter.addFragment(new Fragment_odawise(), "Odako Bibaran");
        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }*/
}
