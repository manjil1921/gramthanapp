package com.pracasinfosys.gramthan.app.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.pracasinfosys.gramthan.app.Adapter.RecyclerView_importantdetails;
import com.pracasinfosys.gramthan.app.Interface_retrofit;
import com.pracasinfosys.gramthan.app.Model.Model_notice_array;
import com.pracasinfosys.gramthan.app.Model.Model_notice_main;
import com.pracasinfosys.gramthan.app.R;
import com.pracasinfosys.gramthan.app.Restapi.Apiclient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Detailsactivity extends AppCompatActivity {
    TextView name,post,phone,maddresss;
    Toolbar mtolbars;
    Interface_retrofit mapi;
             String officeid;
    RecyclerView mrecyclerview;
    RecyclerView.LayoutManager mlinearlayoutmanager;
    String name_api;
    TextView mtolbar_textviewwws;
    List<Model_notice_array> mgetlist = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailsactivity);
        mtolbars = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mtolbars);
        mtolbar_textviewwws = (TextView)findViewById(R.id.mtolbar_textview);
        name_api = getIntent().getExtras().getString("names");
       // setTitle(name_api);
        mtolbar_textviewwws.setText(name_api);
        mrecyclerview = (RecyclerView)findViewById(R.id.mrecyclrviews);
        mlinearlayoutmanager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mrecyclerview.setLayoutManager(mlinearlayoutmanager);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mtolbars.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        name = (TextView)findViewById(R.id.mname);

         phone = (TextView)findViewById(R.id.textview_phone);

         officeid = getIntent().getExtras().getString("officeid");

         getrestapidata();
       // String getpost = getIntent().getExtras().getString("post");
       // String getphone = getIntent().getExtras().getString("phone");
       // String getaddress = getIntent().getExtras().getString("address");
      //  name.setText(getname);
       // post.setText(getpost);
       // phone.setText(getphone);
       // maddresss.setText(getaddress);
    }
    public void getrestapidata(){
        mapi = Apiclient.getApiclient().create(Interface_retrofit.class);
        final Call<Model_notice_main> mgetdetails = mapi.mgetdetailss(officeid);
        mgetdetails.enqueue(new Callback<Model_notice_main>() {
            @Override
            public void onResponse(Call<Model_notice_main> call, Response<Model_notice_main> response) {
                mgetlist = response.body().getResult();
                mrecyclerview.setAdapter(new RecyclerView_importantdetails(getApplicationContext(),mgetlist));
            }

            @Override
            public void onFailure(Call<Model_notice_main> call, Throwable t) {
                Log.d("failed", "onFailure: nnnnnnn"+t.toString());
            }
        });

    }
}
