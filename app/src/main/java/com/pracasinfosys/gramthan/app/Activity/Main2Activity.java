package com.pracasinfosys.gramthan.app.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.pracasinfosys.gramthan.app.R;



public class Main2Activity extends AppCompatActivity{

    WebView wb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        wb = (WebView)findViewById(R.id.webview);
        wb.getSettings().setJavaScriptEnabled(true);
        //wb.setFocusable(true);
        //wb.setFocusableInTouchMode(true);
        //wb.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        //wb.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        //wb.getSettings().setDomStorageEnabled(true);
        //wb.getSettings().setDatabaseEnabled(true);
        //wb.getSettings().setAppCacheEnabled(true);
        //wb.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        //wb.loadData(html, "text/html", null);
        String latitude = getIntent().getExtras().getString("latitude");
        String longitude = getIntent().getExtras().getString("longitude");
        wb.loadUrl("https://www.google.com/maps?q="+"latitude"+"longitude");
       // wb.loadUrl("http://fps.kulchan.com/egov/hello");
        wb.setWebViewClient(new WebViewClient());

    }
}
