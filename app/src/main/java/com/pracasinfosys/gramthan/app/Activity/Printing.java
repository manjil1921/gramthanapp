package com.pracasinfosys.gramthan.app.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.print.PrintManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.pracasinfosys.gramthan.app.Adapter.MyPrintDocumentAdapter;
import com.pracasinfosys.gramthan.app.R;
import com.pracasinfosys.gramthan.app.Utils;
import com.jacksonandroidnetworking.JacksonParserFactory;

/**
 * Created by Razu on 1/23/2018.
 */

public class Printing extends Activity{
    ProgressDialog mdialogue;
    //@Override
    @Override
    public void onBackPressed(){
     finish();
    }
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String getfiles = getIntent().getExtras().getString("files");
        final String getfilename = getIntent().getExtras().getString("filename");
        Log.d("fname", "onCreate: kkkkkkkkkk"+getfilename);
        mdialogue = new ProgressDialog(this);
        progressdialogue("Loading...Please Wait..");
        AndroidNetworking.initialize(this);
        AndroidNetworking.setParserFactory(new JacksonParserFactory());
        AndroidNetworking.download(getfiles, Environment.getExternalStorageDirectory()+"/"+ Utils.downloadDirectory,getfilename)
                .setTag("downloadTest")
                .setPriority(Priority.MEDIUM)
                .build()
                .setDownloadProgressListener(new DownloadProgressListener() {
                    @Override
                    public void onProgress(long bytesDownloaded, long totalBytes) {
                        // do anything with progress
                        //new Handler().postDelayed(new Runnable() {
                            //@Override
                        progressdialogue("Loading...Please Wait..");
                            //public void run() {
                                //Toast.makeText(Printing.this, "Printing..Please Wait..", Toast.LENGTH_SHORT).show();
                          //  }
                        //},5000);

                    }
                })
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        // do anything after completion
                        //
                        mdialogue.dismiss();
                        //finish();
                        Toast.makeText(Printing.this, "Finished", Toast.LENGTH_SHORT).show();
                        PrintManager printManager = (PrintManager)getSystemService(Context.PRINT_SERVICE);
                        String jobName =getString(R.string.app_name) +
                        " Document";
                         printManager.print(jobName, new MyPrintDocumentAdapter(Printing.this,getfilename),null);
                    //
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        mdialogue.dismiss();
                        Toast.makeText(Printing.this, "Failed To Print..Please Try Again Later!!", Toast.LENGTH_SHORT).show();
                        Log.d("ll", "onError: errorddddddddddddddddd"+error.toString());
                    }
                });
    }
    public void progressdialogue(String messages){

        //mdialogue = new ProgressDialog(this);
        mdialogue.setMessage(messages);
        mdialogue.setIndeterminate(false);
        mdialogue.setCancelable(false);
        mdialogue.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mdialogue.setProgress(10);
        mdialogue.show();
       // Handler mahand = new Handler();
        //mahand.postDelayed(new Runnable() {
          //  @Override
            //public void run() {
              //  mdialogue.dismiss();
           //}},4000);
    }
}
