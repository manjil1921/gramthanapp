package com.pracasinfosys.gramthan.app;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pracasinfosys.gramthan.app.Activity.Aboutusactivity;
import com.pracasinfosys.gramthan.app.Activity.Byaktikatghatana_4;
import com.pracasinfosys.gramthan.app.Activity.Gunasotathasujhav_8;
import com.pracasinfosys.gramthan.app.Activity.Kartathasulka_6;
import com.pracasinfosys.gramthan.app.Activity.Mahatyopurnanumber_14;
import com.pracasinfosys.gramthan.app.Activity.Nagarikbadapatra;
import com.pracasinfosys.gramthan.app.Activity.Nagariksurakshya_10;
import com.pracasinfosys.gramthan.app.Activity.Nibedankodhacha;
import com.pracasinfosys.gramthan.app.Activity.Odadetails_all;
import com.pracasinfosys.gramthan.app.Activity.Patrachargarnuhos_7;
import com.pracasinfosys.gramthan.app.Activity.Samajiksurakshya_details;
import com.pracasinfosys.gramthan.app.Activity.Sarbajanickkharid_2;
import com.pracasinfosys.gramthan.app.Activity.Suchanatathasamachar;
import com.pracasinfosys.gramthan.app.Activity.Upavoktasamiti_1;
import com.pracasinfosys.gramthan.app.Adapter.RecyclerView_dashboard;
import com.pracasinfosys.gramthan.app.Model.Model_nagarpalikadashboard;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    RecyclerView views_recycler;
    List<Model_nagarpalikadashboard> mlist;
    RecyclerView.LayoutManager mlayoutmanager;
    RecyclerView_dashboard msemesteradapter;
    Toolbar toolbars;
    TextView mtextviews;
    TextView txt1,txt2,txt3,txt4,txt5,txt6,txt7,txt8,txt9,txt10,txt11,txt12,txt13,txt14;
    ImageView name1,name2,name3,name4,name5,name6,name7,name8,name9,name10,name11,name12,name13,name14;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice);
        toolbars =(Toolbar)findViewById(R.id.ToolBars);
        //recyclerivies =(RecyclerView)findViewById(R.id.recyclerviewss_news);mtextviews = (TextView)toolbars.findViewById(R.id.mtolbar_textview);
        setSupportActionBar(toolbars);
        //setTitle("बुढीगङ्गा गाउपालिका");
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
         //   getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //toolbars.setNavigationOnClickListener(new View.OnClickListener() {
            //@Override
            //public void onClick(View view) {
                //finish();
            //}
        //});
        //text_toolbars.setText(names);
       // mlist = new ArrayList<>();
        //msemesteradapter = new RecyclerView_dashboard(mlist,this);
       // views_recycler =(RecyclerView)findViewById(R.id.recyclerview);
       // views_recycler.setHasFixedSize(true);
       // views_recycler.setNestedScrollingEnabled(false);
      //  mlayoutmanager = new GridLayoutManager(this,2);
      //  views_recycler.setLayoutManager(mlayoutmanager);
      //  views_recycler.setItemAnimator(new DefaultItemAnimator());
      //  views_recycler.setAdapter(msemesteradapter);

       // preparealbmuns();

        name1 = (ImageView)findViewById(R.id.imageview_row1_news);
        Picasso.with(MainActivity.this).load(R.drawable.news).into(name1);
        txt1=(TextView) findViewById(R.id.textview_name_1);

        name2 = (ImageView)findViewById(R.id.imageview_row11publicpurchase);
        Picasso.with(MainActivity.this).load(R.drawable.sarbajanikkharid).into(name2);
        txt2=(TextView)findViewById(R.id.textview_name_2);

        name3 = (ImageView)findViewById(R.id.imageview_name3);
        Picasso.with(MainActivity.this).load(R.drawable.nagarik).into(name3);
        txt3=(TextView)findViewById(R.id.textview_name_3);

        name4 = (ImageView)findViewById(R.id.imageview_name4);
        Picasso.with(MainActivity.this).load(R.drawable.byaktigat).into(name4);
        txt4=(TextView)findViewById(R.id.textview_name_4);

        name5 = (ImageView)findViewById(R.id.imageview_name5);
        Picasso.with(MainActivity.this).load(R.drawable.samajiksurakh).into(name5);
        txt5=(TextView)findViewById(R.id.textview_name_5);

        name6 = (ImageView)findViewById(R.id.imagevewi_name6);
        Picasso.with(MainActivity.this).load(R.drawable.kar).into(name6);
        txt6=(TextView)findViewById(R.id.textview_name_6);

        name7 = (ImageView)findViewById(R.id.imageview_name7);
        Picasso.with(MainActivity.this).load(R.drawable.nibedankodacha).into(name7);
        txt7=(TextView)findViewById(R.id.textview_name_7);

        name8 = (ImageView)findViewById(R.id.iamgeview_name8);
        Picasso.with(MainActivity.this).load(R.drawable.patrachar).into(name8);
        txt8=(TextView)findViewById(R.id.textview_name_8);

        name9 = (ImageView)findViewById(R.id.imageview_name9);
        Picasso.with(MainActivity.this).load(R.drawable.gg).into(name9);
        txt9=(TextView)findViewById(R.id.textview_name_9);

        name10 = (ImageView)findViewById(R.id.imageview_name10);
        Picasso.with(MainActivity.this).load(R.drawable.nagarisuraksha).into(name10);
        txt10=(TextView)findViewById(R.id.textview_name_10);

        name11 = (ImageView)findViewById(R.id.iamgeview_name11);
        Picasso.with(MainActivity.this).load(R.drawable.upavokata).into(name11);
        txt11=(TextView)findViewById(R.id.textview_name_11);

        name12 = (ImageView)findViewById(R.id.iamgeview_name12);
        Picasso.with(MainActivity.this).load(R.drawable.padhadhikari).into(name12);
        txt12=(TextView)findViewById(R.id.textview_name_12);

        name13 = (ImageView)findViewById(R.id.imageview_name13);
        Picasso.with(MainActivity.this).load(R.drawable.samparka).into(name13);
        txt13=(TextView)findViewById(R.id.textview_name_13);

        name14 = (ImageView)findViewById(R.id.imageview_name14);
        Picasso.with(MainActivity.this).load(R.drawable.mahatono).into(name14);
        txt14=(TextView)findViewById(R.id.textview_name_14);


        name1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             Intent mintent = new Intent(MainActivity.this, Suchanatathasamachar.class);
             startActivity(mintent);
            }
        });

        txt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Suchanatathasamachar.class);
                startActivity(mintent);
            }
        });


        name2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Sarbajanickkharid_2.class);
                startActivity(mintent);

            }
        });

        txt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Sarbajanickkharid_2.class);
                startActivity(mintent);
            }
        });

        name3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Nagarikbadapatra.class);
                startActivity(mintent);

            }
        });

        txt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Nagarikbadapatra.class);
                startActivity(mintent);
            }
        });


        name4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Byaktikatghatana_4.class);
                startActivity(mintent);

            }
        });

        txt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Byaktikatghatana_4.class);
                startActivity(mintent);
            }
        });


        name5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Samajiksurakshya_details.class);
                startActivity(mintent);

            }
        });

        txt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Samajiksurakshya_details.class);
                startActivity(mintent);
            }
        });


        name6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Kartathasulka_6.class);
                startActivity(mintent);

            }
        });

        txt6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Kartathasulka_6.class);
                startActivity(mintent);
            }
        });


        name7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Nibedankodhacha.class);
                startActivity(mintent);

            }
        });

        txt7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Nibedankodhacha.class);
                startActivity(mintent);
            }
        });


        name8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Patrachargarnuhos_7.class);
                startActivity(mintent);

            }
        });

        txt8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Patrachargarnuhos_7.class);
                startActivity(mintent);
            }
        });



        name9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Gunasotathasujhav_8.class);
                startActivity(mintent);

            }
        });

        txt9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Gunasotathasujhav_8.class);
                startActivity(mintent);
            }
        });



        name10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Nagariksurakshya_10.class);
                startActivity(mintent);

            }
        });

        txt10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Nagariksurakshya_10.class);
                startActivity(mintent);
            }
        });

        name11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Upavoktasamiti_1.class);
                startActivity(mintent);

            }
        });

        txt11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Upavoktasamiti_1.class);
                startActivity(mintent);
            }
        });


        name12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Odadetails_all.class);
                mintent.putExtra("activity","padhadhikari");
                startActivity(mintent);

            }
        });

        txt12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Odadetails_all.class);
                mintent.putExtra("activity","padhadhikari");
                startActivity(mintent);
            }
        });

        name13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Odadetails_all.class);
                mintent.putExtra("activity","samparka");
                startActivity(mintent);

            }
        });

        txt13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Odadetails_all.class);
                mintent.putExtra("activity","samparka");
                startActivity(mintent);
            }
        });

        name14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Mahatyopurnanumber_14.class);
                startActivity(mintent);

            }
        });

        txt14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(MainActivity.this, Mahatyopurnanumber_14.class);
                startActivity(mintent);
            }
        });
    }

   /* public void preparealbmuns(){
        int[] covers = new int[]{
              // R.string.font_awesome_area_chart_icon,
                //R.string.font_awesome_cubes_icon,
               // R.string.font_awesome_mobile_phone_icon
                R.drawable.newsinfop,R.drawable.publicp,
                R.drawable.list,
                R.drawable.calendar,
                R.drawable.about,
                R.drawable.calendar,
                R.drawable.about,
                R.drawable.patrachar,
                R.drawable.about,
                R.drawable.calendar,
                R.drawable.about,
                R.drawable.mlocation,
                R.drawable.mahatwono
        };
        String[] mtexts = new String[]{
                "सुचना तथा समाचार",
                "सार्बजनिक खरिद ",
                "नागरिक बडापत्र",
                "व्यक्तिगत घटना दर्ता ",
                "सामाजिक सुरक्षा भत्ता",
                "कर तथा शुल्क ",
                "निबेदन को ढाचा",
                "पत्राचार गर्नुहोस ",
                "गुनासो तथा सुझाब",
                "नागरिक सुरक्षा फारम",
                "उपभोक्ता समिती",
                "पधादीकारीको विवरण",
                "सम्पर्क / लोकेसन म्याप ",
                "महत्वपुर्ण नम्बरहरु"
        };
        Model_nagarpalikadashboard cimages = new Model_nagarpalikadashboard(covers[0],mtexts[0]);
        mlist.add(cimages);
        Model_nagarpalikadashboard cimages2 = new Model_nagarpalikadashboard(covers[1],mtexts[1]);
        mlist.add(cimages2);
        Model_nagarpalikadashboard cimages3 = new Model_nagarpalikadashboard(covers[2],mtexts[2]);
        mlist.add(cimages3);
        Model_nagarpalikadashboard cimages4 = new Model_nagarpalikadashboard(covers[3],mtexts[3]);
        mlist.add(cimages4);
        Model_nagarpalikadashboard cimages5 = new Model_nagarpalikadashboard(covers[4],mtexts[4]);
        mlist.add(cimages5);
        Model_nagarpalikadashboard cimages6 = new Model_nagarpalikadashboard(covers[5],mtexts[5]);
        mlist.add(cimages6);
        Model_nagarpalikadashboard cimages7 = new Model_nagarpalikadashboard(covers[6],mtexts[6]);
        mlist.add(cimages7);
        Model_nagarpalikadashboard cimages8 = new Model_nagarpalikadashboard(covers[7],mtexts[7]);
        mlist.add(cimages8);
        Model_nagarpalikadashboard cimages9 = new Model_nagarpalikadashboard(covers[8],mtexts[8]);
        mlist.add(cimages9);
        Model_nagarpalikadashboard cimages0 = new Model_nagarpalikadashboard(covers[9],mtexts[9]);
        mlist.add(cimages0);
        Model_nagarpalikadashboard cimages11 = new Model_nagarpalikadashboard(covers[10],mtexts[10]);
        mlist.add(cimages11);
        Model_nagarpalikadashboard cimages12 = new Model_nagarpalikadashboard(covers[11],mtexts[11]);
        mlist.add(cimages12);
        Model_nagarpalikadashboard cimages13 = new Model_nagarpalikadashboard(covers[12],mtexts[12]);
        mlist.add(cimages13);
        Model_nagarpalikadashboard cimages14 = new Model_nagarpalikadashboard(covers[12],mtexts[13]);
        mlist.add(cimages14);
        msemesteradapter.notifyDataSetChanged();

    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.menu_hamrobarema:
                Intent minten = new Intent(MainActivity.this, Aboutusactivity.class);
                startActivity(minten);
                break;
            case R.id.menudashbaord_rate:
                try {
                    Uri marketUri = Uri.parse("market://details?id="+getPackageName());
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                    startActivity(marketIntent);
                }catch(ActivityNotFoundException e) {
                    Uri marketUri = Uri.parse("https://play.google.com/store/apps/details?id="+getPackageName());
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                    startActivity(marketIntent);
                }
                break;
            case R.id.menu_share:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBodyText = "Check it out. This is the awesome app https://play.google.com/store/apps/details?id="+getPackageName();
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,"About Gramthan Gaupalika app");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBodyText);
                startActivity(Intent.createChooser(sharingIntent, "Sharing Option"));
                break;
            case R.id.menu_exit:
                finish();
                //Intent minten = new Intent(MainActivity.this, Main2Activity.class);
               // startActivity(minten);
                break;
            case R.id.menudashbaord_update:
                try {
                    Uri marketUri = Uri.parse("https://play.google.com/store/apps/details?id="+getPackageName());
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                    startActivity(marketIntent);
                }catch(ActivityNotFoundException e) {
                    Uri marketUri = Uri.parse("https://play.google.com/store/apps/details?id="+getPackageName());
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                    startActivity(marketIntent);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to Exit?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                finish();
            }
        });
        builder.setNeutralButton("Rate Us?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    Uri marketUri = Uri.parse("market://details?id="+getPackageName());
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                    startActivity(marketIntent);
                }catch(ActivityNotFoundException e) {
                    Uri marketUri = Uri.parse("https://play.google.com/store/apps/details?id="+getPackageName());
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                    startActivity(marketIntent);
                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
             dialog.cancel();

            }
        });
      /*  builder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });*/
        AlertDialog alert = builder.create();
        alert.show();
    }

}
