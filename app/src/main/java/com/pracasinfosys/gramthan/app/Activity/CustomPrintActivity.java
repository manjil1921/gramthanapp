package com.pracasinfosys.gramthan.app.Activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.pdf.PdfDocument;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.PrintManager;
import android.print.pdf.PrintedPdfDocument;

import com.pracasinfosys.gramthan.app.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by user on 1/22/2018.
 */

public class CustomPrintActivity extends Activity {

    private Context mcontext;
    public CustomPrintActivity(Context mcontext) {
        this.mcontext = mcontext;
    }

    //String filenames = getIntent().getExtras().getString("filenames");

    //PrintManager printManager = (PrintManager)getSystemService(Context.PRINT_SERVICE);
    //String jobName = getString(R.string.app_name) +
            //" Document";
    //printManager.print(jobName, new MyPrintDocumentAdapter(context,downloadFileName),null);
    public class MyPrintDocumentAdapter extends PrintDocumentAdapter {
        Context mcontext;
        private int pageHeight;
        private int pageWidth;
        public PdfDocument myPdfDocument;
        public int totalpages = 104;
       String downloadfilename;

        public MyPrintDocumentAdapter(Context mcontext,String downloadfilename) {
            this.mcontext = mcontext;
            this.downloadfilename = downloadfilename;
        }

        @Override
        public void onLayout(PrintAttributes printAttributes, PrintAttributes printAttributes1, CancellationSignal cancellationSignal, LayoutResultCallback layoutResultCallback, Bundle bundle) {
            myPdfDocument =  new PrintedPdfDocument(mcontext,printAttributes1);
            pageHeight =
                    printAttributes1.getMediaSize().getHeightMils()/1000 * 72;
            pageWidth =
                    printAttributes1.getMediaSize().getWidthMils()/1000 * 72;
            if(cancellationSignal.isCanceled()){
                layoutResultCallback.onLayoutCancelled();
                return;
            }
            if(totalpages>0){
                PrintDocumentInfo.Builder builder = new PrintDocumentInfo
                        .Builder("Budiganga.pdf")
                        .setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT);
                        //.setPageCount(totalpages);
                //.build();
                PrintDocumentInfo info = builder.build();
                layoutResultCallback.onLayoutFinished(info, true);
            }else{
                layoutResultCallback.onLayoutFailed("Page count is zero.");
            }

        }

        @Override
        public void onWrite(PageRange[] pageRanges, ParcelFileDescriptor parcelFileDescriptor, CancellationSignal cancellationSignal, WriteResultCallback writeResultCallback) {
            InputStream input = null;
            OutputStream output = null;
            try {
                input = new FileInputStream(new File(Environment.getExternalStorageDirectory(),downloadfilename));
                output = new FileOutputStream(parcelFileDescriptor.getFileDescriptor());

                byte[] buf = new byte[1024];
                int bytesRead;

                while ((bytesRead = input.read(buf)) > 0) {
                    output.write(buf, 0, bytesRead);
                }
                //myPdfDocument.writeTo(new FileOutputStream(parcelFileDescriptor.getFileDescriptor()));
            } catch (IOException e) {
                writeResultCallback.onWriteFailed(e.toString());
                return;
            } finally {
                myPdfDocument.close();
                myPdfDocument = null;

            }

            writeResultCallback.onWriteFinished(pageRanges);


        }
    }
public void printss(String filenames){

    PrintManager printManager = (PrintManager)getSystemService(Context.PRINT_SERVICE);
    String jobName = getString(R.string.app_name) +
            " Document";
    printManager.print(jobName, new MyPrintDocumentAdapter(CustomPrintActivity.this,filenames),null);
}

}
