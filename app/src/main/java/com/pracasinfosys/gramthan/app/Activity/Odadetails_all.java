package com.pracasinfosys.gramthan.app.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.pracasinfosys.gramthan.app.Adapter.UsersAdapter;
import com.pracasinfosys.gramthan.app.Interface_retrofit;
import com.pracasinfosys.gramthan.app.Model.Model_notice_array;
import com.pracasinfosys.gramthan.app.Model.Model_notice_main;
import com.pracasinfosys.gramthan.app.R;
import com.pracasinfosys.gramthan.app.Restapi.Apiclient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Odadetails_all extends AppCompatActivity {
    Toolbar mtolbars;
    TextView mtext1,mtext2,mtext3,mtext4,mtext5,mtextgaupalika;
    String getactivity;
    Interface_retrofit minterface;
    List<Model_notice_array> mlistss = new ArrayList<>();
    LinearLayout mlinearlayout,linear_gaupalika,linear1,linear2,linear3,linear4,linear5;
    ListView mlistviews;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_odadetails_all);
        mtolbars = (Toolbar) findViewById(R.id.toolbar);
        //mrecycler=(RecyclerView)findViewById(R.id.mrecyclerview);
        //mswiperefresh =(SwipeRefreshLayout)findViewById(R.id.mswiprefresh);
        //mswiperefresh.setRefreshing(true);
//        mtext1 = (TextView) findViewById(R.id.textview_details_woda1);
//        linear1 = (LinearLayout) findViewById(R.id.linear_woda1);
//        mtext2 = (TextView) findViewById(R.id.textview_details_woda2);
//        linear2 = (LinearLayout) findViewById(R.id.linear_woda2);
//        mtext3 = (TextView) findViewById(R.id.textview_details_woda3);
//        linear3 = (LinearLayout) findViewById(R.id.linear_woda3);
//        mtext4 = (TextView) findViewById(R.id.textview_details_woda4);
//        linear4 = (LinearLayout) findViewById(R.id.linear_woda4);
//        mtext5 = (TextView) findViewById(R.id.textview_details_woda5);
//        linear5 = (LinearLayout) findViewById(R.id.linear_woda5);
//        mtextgaupalika = (TextView) findViewById(R.id.textview_details_gaupalika);
//        mlinearlayout = (LinearLayout) findViewById(R.id.linear_woda);
        mlistviews = (ListView) findViewById(R.id.mlistview);


        //names = getIntent().getExtras().getString("name_1");
        setSupportActionBar(mtolbars);
        //setTitle("वडाको विवरण ");
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mtolbars.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        getactivity = getIntent().getExtras().getString("activity");
        if(getactivity.equals("samajik_surakshya")){
            getrestdata_samachar();

        }else{
            getrestdata_wadasamachar();
        }

        mlistviews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
               // Toast.makeText(Odadetails_all.this, "Your click "+adapterView.getItemAtPosition(i), Toast.LENGTH_SHORT).show();
                Log.d("oda", "onItemClick: nnnnnnnnnitemsssssssssss"+i+"dfd"+adapterView.getItemIdAtPosition(i));
                int id = i +1;
                String ids_padahasamparka = Integer.toString(i);
                String ids_samajik = Integer.toString(id);
                if(getactivity.equals("samajik_surakshya")){
                    Intent minten = new Intent(Odadetails_all.this, Samajiksurakshya_5.class);
                      minten.putExtra("oda", ids_samajik);
                         startActivity(minten);

                }else if(getactivity.equals("padhadhikari")){
                    Intent minten = new Intent(Odadetails_all.this, Padadhikairkobibaran_12.class);
                       minten.putExtra("oda", ids_padahasamparka);
                       startActivity(minten);
                }else{
                    Intent  min = new Intent(Odadetails_all.this,Samparka_13.class);
                          min.putExtra("oda",ids_padahasamparka);
                             startActivity(min);
                }
            }
        });
    }
    public void getrestdata_samachar(){
        minterface= Apiclient.getApiclient().create(Interface_retrofit.class);
        Call<Model_notice_main> mnotice = minterface.mgetwardsofficeial();
        mnotice.enqueue(new Callback<Model_notice_main>() {
            @Override
            public void onResponse(Call<Model_notice_main> call, Response<Model_notice_main> response) {
                mlistss = response.body().getResult();
                Log.d("faileds", "onFailure: failesd"+mlistss);
                //mrecycler.setAdapter(new Duhabi_newsadapter(getApplicationContext(),mlistss));
//                ArrayAdapter<List> madapter=new ArrayAdapter<List>(this, android.R.layout.simple_list_item_1, mlistss);
               // mrecyclerview.setAdapter(new RecyclerView_odadetails(getApplicationContext(),mlistss,getactivity));
                // mlists.setAdapter(new UsersAdapter(getApplicationContext(),mlistss));
               // ArrayAdapter<List<Model_notice_array>> madapter = new ArrayAdapter<List<Model_notice_array>>(this,android.R.layout.simple_list_item_1,mlistss);
             // mgetward.setAdapter(new Recyclerview_Suchanasamachar(mlistss,getApplicationContext()));
                mlistviews.setAdapter(new UsersAdapter(getApplicationContext(),mlistss,getactivity));

            }

            @Override
            public void onFailure(Call<Model_notice_main> call, Throwable t) {
                Log.d("failed", "onFailure: failesd"+t.toString());
            }
        });


    }

    public void getrestdata_wadasamachar(){
        minterface= Apiclient.getApiclient().create(Interface_retrofit.class);
        Call<Model_notice_main> mnotice = minterface.mgetward_samparka();
        mnotice.enqueue(new Callback<Model_notice_main>() {
            @Override
            public void onResponse(Call<Model_notice_main> call, Response<Model_notice_main> response) {
                mlistss = response.body().getResult();
                Log.d("faileds", "onFailure: failesd"+mlistss);
                //mrecycler.setAdapter(new Duhabi_newsadapter(getApplicationContext(),mlistss));
//                ArrayAdapter<List> madapter=new ArrayAdapter<List>(this, android.R.layout.simple_list_item_1, mlistss);
                // mrecyclerview.setAdapter(new RecyclerView_odadetails(getApplicationContext(),mlistss,getactivity));
                // mlists.setAdapter(new UsersAdapter(getApplicationContext(),mlistss));
                // ArrayAdapter<List<Model_notice_array>> madapter = new ArrayAdapter<List<Model_notice_array>>(this,android.R.layout.simple_list_item_1,mlistss);
                // mgetward.setAdapter(new Recyclerview_Suchanasamachar(mlistss,getApplicationContext()));
                mlistviews.setAdapter(new UsersAdapter(getApplicationContext(),mlistss,getactivity));
            }

            @Override
            public void onFailure(Call<Model_notice_main> call, Throwable t) {
                Log.d("failed", "onFailure: failesd"+t.toString());
            }
        });


    }


//        if (getactivity.equals("padhadhikari") || getactivity.equals("samparka")){
//            //mtextgaupalika.setVisibility(View.VISIBLE);
//            mlinearlayout.setVisibility(View.VISIBLE);
//            mlinearlayout.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(getactivity.equals("padhadhikari")) {
//                        Intent minten = new Intent(Odadetails_all.this, Padadhikairkobibaran_12.class);
//                        minten.putExtra("oda", "0");
//                        startActivity(minten);
//                    }else if(getactivity.equals("samparka")){
//                        Intent  min = new Intent(Odadetails_all.this,Samparka_13.class);
//                        min.putExtra("oda","0");
//                        startActivity(min);
//                    }
//                }
//            });
//            mtextgaupalika.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(getactivity.equals("padhadhikari")) {
//                        Intent minten = new Intent(Odadetails_all.this, Padadhikairkobibaran_12.class);
//                        minten.putExtra("oda", "0");
//                        startActivity(minten);
//                    }else if(getactivity.equals("samparka")){
//                        Intent  min = new Intent(Odadetails_all.this,Samparka_13.class);
//                        min.putExtra("oda","0");
//                        startActivity(min);
//                    }
//                }
//            });
//        }
//       linear1.setOnClickListener(new View.OnClickListener() {
//           @Override
//           public void onClick(View view) {
//               if (getactivity.equals("samajik_surakshya")) {
//                   Intent minten = new Intent(Odadetails_all.this, Samajiksurakshya_5.class);
//                   minten.putExtra("oda", "1");
//                   startActivity(minten);
//               }else if(getactivity.equals("padhadhikari")){
//                   Intent minten = new Intent(Odadetails_all.this,Padadhikairkobibaran_12.class);
//                   minten.putExtra("oda","1");
//                   startActivity(minten);
//
//               }else{
//                   Intent  min = new Intent(Odadetails_all.this,Samparka_13.class);
//                   min.putExtra("oda","1");
//                   startActivity(min);
//               }
//
//           }
//       });
//        mtext1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (getactivity.equals("samajik_surakshya")) {
//                    Intent minten = new Intent(Odadetails_all.this, Samajiksurakshya_5.class);
//                    minten.putExtra("oda", "1");
//                    startActivity(minten);
//                }else if(getactivity.equals("padhadhikari")){
//                    Intent minten = new Intent(Odadetails_all.this,Padadhikairkobibaran_12.class);
//                    minten.putExtra("oda","1");
//                    startActivity(minten);
//
//                }else{
//                    Intent  min = new Intent(Odadetails_all.this,Samparka_13.class);
//                    min.putExtra("oda","1");
//                    startActivity(min);
//                }
//
//            }
//        });
//        linear2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (getactivity.equals("samajik_surakshya")) {
//                    Intent minten = new Intent(Odadetails_all.this, Samajiksurakshya_5.class);
//                    minten.putExtra("oda", "2");
//                    startActivity(minten);
//                }else if(getactivity.equals("padhadhikari")){
//                    Intent minten = new Intent(Odadetails_all.this,Padadhikairkobibaran_12.class);
//                    minten.putExtra("oda","2");
//                    startActivity(minten);
//
//                }else{
//                    Intent  min = new Intent(Odadetails_all.this,Samparka_13.class);
//                    min.putExtra("oda","2");
//                    startActivity(min);
//                }
//            }
//        });
//        mtext2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (getactivity.equals("samajik_surakshya")) {
//                    Intent minten = new Intent(Odadetails_all.this, Samajiksurakshya_5.class);
//                    minten.putExtra("oda", "2");
//                    startActivity(minten);
//                }else if(getactivity.equals("padhadhikari")){
//                    Intent minten = new Intent(Odadetails_all.this,Padadhikairkobibaran_12.class);
//                    minten.putExtra("oda","2");
//                    startActivity(minten);
//
//                }else{
//                    Intent  min = new Intent(Odadetails_all.this,Samparka_13.class);
//                    min.putExtra("oda","2");
//                    startActivity(min);
//                }
//            }
//        });
//        linear3.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (getactivity.equals("samajik_surakshya")) {
//                    Intent minten = new Intent(Odadetails_all.this, Samajiksurakshya_5.class);
//                    minten.putExtra("oda", "3");
//                    startActivity(minten);
//                }else if(getactivity.equals("padhadhikari")){
//                    Intent minten = new Intent(Odadetails_all.this,Padadhikairkobibaran_12.class);
//                    minten.putExtra("oda","3");
//                    startActivity(minten);
//
//                }else{
//                    Intent  min = new Intent(Odadetails_all.this,Samparka_13.class);
//                    min.putExtra("oda","3");
//                    startActivity(min);
//                }
//            }
//        });
//        mtext3.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (getactivity.equals("samajik_surakshya")) {
//                    Intent minten = new Intent(Odadetails_all.this, Samajiksurakshya_5.class);
//                    minten.putExtra("oda", "3");
//                    startActivity(minten);
//                }else if(getactivity.equals("padhadhikari")){
//                    Intent minten = new Intent(Odadetails_all.this,Padadhikairkobibaran_12.class);
//                    minten.putExtra("oda","3");
//                    startActivity(minten);
//
//                }else{
//                    Intent  min = new Intent(Odadetails_all.this,Samparka_13.class);
//                    min.putExtra("oda","3");
//                    startActivity(min);
//                }
//            }
//        });
//        linear4.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (getactivity.equals("samajik_surakshya")) {
//                    Intent minten = new Intent(Odadetails_all.this, Samajiksurakshya_5.class);
//                    minten.putExtra("oda", "4");
//                    startActivity(minten);
//                }else if(getactivity.equals("padhadhikari")){
//                    Intent minten = new Intent(Odadetails_all.this,Padadhikairkobibaran_12.class);
//                    minten.putExtra("oda","4");
//                    startActivity(minten);
//
//                }else{
//                    Intent  min = new Intent(Odadetails_all.this,Samparka_13.class);
//                    min.putExtra("oda","4");
//                    startActivity(min);
//                }
//            }
//        });
//        mtext4.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (getactivity.equals("samajik_surakshya")) {
//                    Intent minten = new Intent(Odadetails_all.this, Samajiksurakshya_5.class);
//                    minten.putExtra("oda", "4");
//                    startActivity(minten);
//                }else if(getactivity.equals("padhadhikari")){
//                    Intent minten = new Intent(Odadetails_all.this,Padadhikairkobibaran_12.class);
//                    minten.putExtra("oda","4");
//                    startActivity(minten);
//
//                }else{
//                    Intent  min = new Intent(Odadetails_all.this,Samparka_13.class);
//                    min.putExtra("oda","4");
//                    startActivity(min);
//                }
//            }
//        });
//        linear5.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (getactivity.equals("samajik_surakshya")) {
//                    Intent minten = new Intent(Odadetails_all.this, Samajiksurakshya_5.class);
//                    minten.putExtra("oda", "5");
//                    startActivity(minten);
//                }else if(getactivity.equals("padhadhikari")){
//                    Intent minten = new Intent(Odadetails_all.this,Padadhikairkobibaran_12.class);
//                    minten.putExtra("oda","5");
//                    startActivity(minten);
//
//                }else{
//                    Intent  min = new Intent(Odadetails_all.this,Samparka_13.class);
//                    min.putExtra("oda","6");
//                    startActivity(min);
//                }
//            }
//        });
//        mtext5.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (getactivity.equals("samajik_surakshya")) {
//                    Intent minten = new Intent(Odadetails_all.this, Samajiksurakshya_5.class);
//                    minten.putExtra("oda", "5");
//                    startActivity(minten);
//                }else if(getactivity.equals("padhadhikari")){
//                    Intent minten = new Intent(Odadetails_all.this,Padadhikairkobibaran_12.class);
//                    minten.putExtra("oda","5");
//                    startActivity(minten);
//
//                }else{
//                    Intent  min = new Intent(Odadetails_all.this,Samparka_13.class);
//                    min.putExtra("oda","6");
//                    startActivity(min);
//                }
//            }
//        });
//    }
    }

