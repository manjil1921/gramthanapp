package com.pracasinfosys.gramthan.app.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.pracasinfosys.gramthan.app.R;

public class Nagarikbadapatra_detials extends AppCompatActivity {
    TextView details,dates,titless;
    Toolbar mtoolbar;
    //String gettitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nagarikbadapatra_detials);
        mtoolbar = (Toolbar)findViewById(R.id.toolbar);
        details = (TextView)findViewById(R.id.textview_detaislnoticedetails);
        titless = (TextView)findViewById(R.id.textview_detailsnotices);
        setSupportActionBar(mtoolbar);
        //gettitle = getIntent().getExtras().getString("titles");
        //Log.d("v", "onCreate: vvvvvvvvvvv"+gettitle);
       // setTitle(gettitle);
       // if(gettitle.equals("सुरक्षा भत्ता बारे जानकारी")){
            //titless.setVisibility(View.GONE);
      //  }else {
           // titless.setVisibility(View.VISIBLE);
        //}
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mtoolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        String detials = getIntent().getExtras().getString("content");
        String titles = getIntent().getExtras().getString("title");
        details.setText(detials);
        //dates.setText(datess);
        titless.setText(titles);
    }
}
