package com.pracasinfosys.gramthan.app.Activity;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.pracasinfosys.gramthan.app.Adapter.Recyclerview_socialsecurity;
import com.pracasinfosys.gramthan.app.Interface_retrofit;
import com.pracasinfosys.gramthan.app.Model.Model_notice_array;
import com.pracasinfosys.gramthan.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Samajiksurakshya_details extends AppCompatActivity {
    Interface_retrofit minterface;
    RecyclerView mrecycler;
    RecyclerView.LayoutManager mlayoutmanager;
    Recyclerview_socialsecurity madapter;
    SwipeRefreshLayout mswiperefresh;
    List<Model_notice_array> mlist_samajik = new ArrayList<>();
    Toolbar mtolbars;
    TextView mtextviwe1,mtetview2;
    RequestQueue mrequest;
    LinearLayout layout1,layout2;
    Model_notice_array marray = new Model_notice_array();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_samajiksurakshya_details);
        mtolbars = (Toolbar)findViewById(R.id.toolbar);
        //mrecycler=(RecyclerView)findViewById(R.id.mrecyclerview);
        //mswiperefresh =(SwipeRefreshLayout)findViewById(R.id.mswiprefresh);
        //mswiperefresh.setRefreshing(true);
        layout1=(LinearLayout)findViewById(R.id.mlinear1);
        layout2 =(LinearLayout)findViewById(R.id.mlinear_2);
        mtextviwe1 = (TextView)findViewById(R.id.textview_details_samajik1);
        mtetview2=(TextView)findViewById(R.id.textview_details_samajik2);
        mtextviwe1.setText(R.string.samajik_details_1);
        mtetview2.setText("सुरक्षा भत्ता बारे जानकारी ");
        //names = getIntent().getExtras().getString("name_1");
        setSupportActionBar(mtolbars);
        //setTitle("सामाजिक सुरक्षा भत्ता");
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mtolbars.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mrequest = Volley.newRequestQueue(this);
        getdata_voolley();
        layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(Samajiksurakshya_details.this,Odadetails_all.class);
                mintent.putExtra("activity","samajik_surakshya");
                startActivity(mintent);
            }
        });
        mtextviwe1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mintent = new Intent(Samajiksurakshya_details.this,Odadetails_all.class);
                mintent.putExtra("activity","samajik_surakshya");
                startActivity(mintent);
            }
        });
       // getreestapi_sarbajanikkharid();
        layout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent minten = new Intent(Samajiksurakshya_details.this,Activity_2sarbajanik.class);
                //minten.putExtra("titles","सुरक्षा भत्ता बारे जानकारी ");
                //minten.putExtra("title","सामाजिक सुरक्षा भत्ता");
                minten.putExtra("content",marray.getAbout());
                //Log.d("about", "onClick: bbbbbbbbbbbbbbbbbbb"+marrays.getAbout());
                startActivity(minten);
            }
        });
        mtetview2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Model_notice_array marrays = new Model_notice_array();

                Intent minten = new Intent(Samajiksurakshya_details.this,Activity_2sarbajanik.class);
                //minten.putExtra("titles","सुरक्षा भत्ता बारे जानकारी ");
                //minten.putExtra("title","सामाजिक सुरक्षा भत्ता");
                minten.putExtra("content",marray.getAbout());
                //Log.d("about", "onClick: bbbbbbbbbbbbbbbbbbb"+marrays.getAbout());
                startActivity(minten);
            }
        });
    }
       /* mswiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mswiperefresh.setRefreshing(true);
                if(isconnectd()){
                    //getreestapi_sarbajanikkharid();
                    Toast.makeText(Samajiksurakshya_details.this,"Refresh Successfull",Toast.LENGTH_SHORT).show();
                    mswiperefresh.setRefreshing(false);
                }else{
                    //mlinear.setVisibility(View.VISIBLE);
                    Toast.makeText(Samajiksurakshya_details.this,"Refresh Failed",Toast.LENGTH_SHORT).show();
                    mswiperefresh.setRefreshing(false);
                }

            }
        });
        mlayoutmanager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mrecycler.setLayoutManager(mlayoutmanager);
        //getreestapi_sarbajanikkharid();
    }
    public boolean isconnectd(){
        ConnectivityManager mconn = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo minfo =mconn.getActiveNetworkInfo();
        if(minfo != null && minfo.isConnected()){
            return true;
        }
        return false;
    }*/
    /*public void getreestapi_sarbajanikkharid(){
        minterface = Apiclient.getApiclient().create(Interface_retrofit.class);
        Call<Model_notice_main> msarbajanik = minterface.mgetsecutity_details();
        msarbajanik.enqueue(new Callback<Model_notice_main>() {
            @Override
            public void onResponse(Call<Model_notice_main> call, Response<Model_notice_main> response) {
                mlist_samajik = response.body().getResult();
                Log.d("suc", "onResponse: kkkkkkkkkkk"+mlist_samajik);
               // mrecycler.setAdapter(new Recyclerview_socialsecurity(mlist_samajik,getApplicationContext()));

            }

            @Override
            public void onFailure(Call<Model_notice_main> call, Throwable t) {
                Log.d("failed", "onResponse: kkkkkkkkkkk"+t.toString());
            }
        });

    }*/
       public void getdata_voolley(){
           String urls = "https://pracasinfosys.com/gramthan/api/getSocialSecurityDetail";
           JsonObjectRequest requestMessage = new JsonObjectRequest(Request.Method.GET, urls, new com.android.volley.Response.Listener<JSONObject>() {
               @Override
               public void onResponse(JSONObject response) {
                   Log.v("About", response.toString());

                   //readWrite.writeFile("About.txt", response);
                   //Pref.sSavePreferences(getApplicationContext(),"About.txt", true);
                   parseAndDisplayMessage(response);

                   // final JSONObject result;
                   // try {
                   //  result = response.getJSONObject("result");
                   // emails = result.getString("email");
                   //Log.d("About", "onResponse: lllllllllllllllll"+emails);
                   // } catch (JSONException e) {
                   // e.printStackTrace();
                   //  }

                   // String body = result.getString("body");
                   // String subTitle = result.getString("sub_title");
                   // messageTitle.setText(Html.fromHtml(title));
                   //messageBody.setText(Html.fromHtml(body));
                   //messageSender.setText(Html.fromHtml(subTitle));




               }
           }, new com.android.volley.Response.ErrorListener() {
               @Override
               public void onErrorResponse(VolleyError error) {

               }
           });

           mrequest.add(requestMessage);

       }
    public void parseAndDisplayMessage(JSONObject response)
    {


        try {
            JSONArray array = response.getJSONArray("result");

            // emails = result.getString("email");
            //Log.d("About", "onResponse: lllllllllllllllll"+emails);
            //String body = result.getString("body");
            // String subTitle = result.getString("sub_title");
            for(int i = 0; i<array.length(); i++)
            {

                JSONObject obj = array.getJSONObject(i);

                //Model_notice_array ema = new Model_notice_array(obj.getString("email"));
                Log.d("About", "onResponse: lllllllllllllllll"+obj.getString("about"));
                //mlistemail.add(ema);
                //list.add("Select Email to Send");
                //list.add(obj.getString("email"));
                marray.setAbout(obj.getString("about"));

            }







        } catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

}


