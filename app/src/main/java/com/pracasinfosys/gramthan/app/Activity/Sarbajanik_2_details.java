package com.pracasinfosys.gramthan.app.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.pracasinfosys.gramthan.app.Downloadclass;
import com.pracasinfosys.gramthan.app.R;
import com.pracasinfosys.gramthan.app.Utils;

//import static com.example.razu.egovernanceapp.Utils.downloadPdfUrl;

public class Sarbajanik_2_details extends AppCompatActivity {
ImageView mimageview;
TextView mtextviews,tolbartextview;
    Button mntm,downloadbtn;
    Toolbar mtolbars;
    Downloadclass mclass ;
    ProgressDialog mdialogue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sarbajanik_2_details);
        mtolbars = (Toolbar)findViewById(R.id.toolbar);
        mimageview = (ImageView)findViewById(R.id.imageview_detailsnotices);
        mtextviews =(TextView)findViewById(R.id.textview_detaislnoticedetails_sarbajanik);
        tolbartextview = (TextView)findViewById(R.id.mtolbar_textview);
        setSupportActionBar(mtolbars);
       // setTitle("सार्बजनिक खरिद/बोलपत्र सुचना");
        AndroidNetworking.initialize(this);
        mdialogue = new ProgressDialog(this);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mtolbars.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        final String details = getIntent().getExtras().getString("content");
       final String files = getIntent().getExtras().getString("file");
       final String downloadfilename = getIntent().getExtras().getString("downloadfile");
       final String tolbartext = getIntent().getExtras().getString("tolbar_text");
        mtextviews.setText(details);
        tolbartextview.setText(tolbartext);
        //final String mstrin = "publicpurchase";
        final String downloadPdfUrl = "http://demo.acepirit.com/bsccsit/uploads/syllabus/0IWlu783.pdf";
        mntm =(Button)findViewById(R.id.btn_viewpdf);
        downloadbtn = (Button)findViewById(R.id.downloadpdfbtn);

        if(files.equals("Nofile")){
            mntm.setVisibility(View.GONE);
            downloadbtn.setVisibility(View.GONE);

        }else{


        downloadbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(Sarbajanik_2_details.this, "Downloading..Please Wait...", Toast.LENGTH_LONG).show();
//               if(files.equals("Nofile")){
//                   Toast.makeText(Sarbajanik_2_details.this, "There is no File", Toast.LENGTH_SHORT).show();
//return;
//               }else{
//               //mclass = new Downloadclass(Sarbajanik_2_details.this,files,"publicpurchase");
               // Intent minten = new Intent(Sarbajanik_2_details.this,Pdfviews.class);
               // minten.putExtra("file",downloadPdfUrl);
                //startActivity(minten);
                AndroidNetworking.download(files, Environment.getExternalStorageDirectory()+"/"+ Utils.downloadDirectory,downloadfilename)
                        .setTag("downloadTest")
                        .setPriority(Priority.MEDIUM)
                        .build()
                        .setDownloadProgressListener(new DownloadProgressListener() {
                            @Override
                            public void onProgress(long bytesDownloaded, long totalBytes) {
                                // do anything with progress
                                //new Handler().postDelayed(new Runnable() {
                                    //@Ov//erride
                                   // public void run() {
                                progressdialogue("Downloading...Please Wait..");
                                       // Toast.makeText(Sarbajanik_2_details.this, "Printing..Please Wait..", Toast.LENGTH_SHORT).show();
                                  //  }
                             //   },5000);

                            }
                        })
                        .startDownload(new DownloadListener() {
                            @Override
                            public void onDownloadComplete() {
                                // do anything after completion
                                mdialogue.dismiss();
                                Toast.makeText(Sarbajanik_2_details.this, "Download Finished", Toast.LENGTH_SHORT).show();
                                //PrintManager printManager = (PrintManager)getSystemService(Context.PRINT_SERVICE);
                                //String jobName =getString(R.string.app_name) +
                                       // " Document";
                                //printManager.print(jobName, new MyPrintDocumentAdapter(Sarbajanik_2_details.this,getfilename),null);
                                //finish();

                            }
                            @Override
                            public void onError(ANError error) {
                                // handle error
                                mdialogue.dismiss();
                                Toast.makeText(Sarbajanik_2_details.this, "Failed To Download..Please Try Again Later!!", Toast.LENGTH_SHORT).show();
                                Log.d("ll", "onError: errorddddddddddddddddd"+error.toString());
                            }
                        });
                  }
        });

        mntm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent minent = new Intent(Sarbajanik_2_details.this,Pdfviews.class);
               // minent.putExtra("file_url",files);
                //startActivity(minent);
               // mclass = new Downloadclass(getApplicationContext(),files,"publicpurchase");
                /*File apkStorage = new File(
                        Environment.getExternalStorageDirectory() + "/"
                                + Utils.downloadDirectory);
                Ion.with(getApplicationContext())
                        .load(files)
                        .write(new File(Environment.getExternalStorageDirectory() + "/"
                                + Utils.downloadDirectory))
                        .setCallback(new FutureCallback<File>() {
                            @Override
                            public void onCompleted(Exception e, File file) {
                                // download done...
                                // do stuff with the File or error
                                Toast.makeText(Sarbajanik_2_details.this, "successsssssss", Toast.LENGTH_SHORT).show();
                            }
                        });*/
//                if(files.equals("Nofile")){
//                    Toast.makeText(Sarbajanik_2_details.this, "There is no File", Toast.LENGTH_SHORT).show();
//                  return;
//                }else{
                      Intent minten = new Intent(Intent.ACTION_VIEW,Uri.parse(files));
                    startActivity(minten);

             //   }

            }
        });
        }

    }
    public void progressdialogue(String messages){

        //mdialogue = new ProgressDialog(this);
        mdialogue.setMessage(messages);
        mdialogue.setIndeterminate(false);
        mdialogue.setCancelable(false);
        mdialogue.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mdialogue.setProgress(10);
        mdialogue.show();
        Handler mahand = new Handler();
        mahand.postDelayed(new Runnable() {
            @Override
            public void run() {
                mdialogue.dismiss();
            }
        },3000);
    }


}
