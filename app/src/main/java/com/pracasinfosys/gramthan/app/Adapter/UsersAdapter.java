package com.pracasinfosys.gramthan.app.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pracasinfosys.gramthan.app.Model.Model_notice_array;
import com.pracasinfosys.gramthan.app.R;

import java.util.List;

/**
 * Created by Paru on 2/22/2018.
 */

public class UsersAdapter extends BaseAdapter {

    private Context mContext;
    List<Model_notice_array> marray;
    String activity;

    public UsersAdapter(Context mContext, List<Model_notice_array> marray,String activity) {
        this.mContext = mContext;
        this.marray = marray;
        this.activity = activity;
    }
    //    private final String[] web;
//    private final int[] Imageid;
//
//    public UsersAdapter(Context c, String[] web, int[] Imageid ) {
//        mContext = c;
//        this.Imageid = Imageid ;
//        this.web = web;
//
//    }

    @Override
    public int getCount() {

        return marray.size();

    }


    @Override
    public Object getItem(int position) {

        return null;
    }

    @Override
    public long getItemId(int position) {

        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View grid;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            grid = inflater.inflate(R.layout.activity_listview, null);


        }

        else {
            grid = convertView;

        }
        TextView textView = (TextView) grid.findViewById(R.id.mtextviews);
//        ImageView imageView = (ImageView)grid.findViewById(R.id.grid_image);
        textView.setText(marray.get(position).getName());

//        imageView.setImageResource(Imageid[position]);


        return grid;
    }















//public class UsersAdapter extends ArrayAdapter<Model_notice_array> {
//    Context context;
//    List<Model_notice_array> users;
//    public UsersAdapter(Context context, List<Model_notice_array> users) {
//        super(context, 0, users);
//
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        // Get the data item for this position
//        //Model_notice_array user = users.get(position);
//        // Check if an existing view is being reused, otherwise inflate the view
//        if (convertView == null) {
//            convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_list, parent, false);
//        }
//        // Lookup view for data population
//        TextView textview_details_woda1 = (TextView) convertView.findViewById(R.id.textview_details_woda1);
////        TextView tvHome = (TextView) convertView.findViewById(R.id.tvHome);
//        // Populate the data into the template view using the data object
//        textview_details_woda1.setText(users.get(position).getName());
//
//        // Return the completed view to render on screen
//        return convertView;
//    }
}
